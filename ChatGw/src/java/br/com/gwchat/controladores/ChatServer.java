package br.com.gwchat.controladores;

import br.com.gwchat.apoio.Auxiliar;
import br.com.gwchat.apoio.GerarConexao;
import br.com.gwchat.bo.GwChatBO;
import br.com.gwchat.geral.Cliente;
import br.com.gwchat.geral.Ocorrencia;
import br.com.gwchat.geral.Suporte;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author Mateus
 */
@ServerEndpoint(value = "/chatServer")
public class ChatServer {

    //MSG CLIENTE
    final static String MSG_FILA_ESPERA = Auxiliar.getHoraAtual() + "!@!" + "Gw Chat #@# " + "Seu atendimento foi registrado, favor aguardar. Sua posi��o �: ";
    final static String MSG_SAUDACAO = Auxiliar.getHoraAtual() + "!@!<nmSuporte>#@# " + Auxiliar.getSaudacao() + " Sr(a) <nmCliente>, eu sou <nmSuporte> e o protocolo desse atendimento � <N� Protocolo>, em que posso ajudar?";
    final static String MSG_GANHOU_POSICAO = Auxiliar.getHoraAtual() + "!@!" + "Gw Chat #@# " + "Sua posi��o agora �:";
    //MSG SUPORTE
    final static String MSG_SUPORTE_INICIO_ATENDIMENTO = Auxiliar.getHoraAtual() + "!@!<nmSuporte>#@# Atendimento iniciado com o cliente <nmClienteBase> <br> Contato: <nmCliente> <br>  Protocolo: <N� Protocolo>.";
    final static String CLIENTE_DESCONECTOU = Auxiliar.getHoraAtual() + "!@!<nmSuporte>#@# Cliente desconectou da conversa";
    //MSG GERAL
    //SALAS CLIENTE
    public static Map<String, Session> filaEsperaAtendimentoTrans = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> filaEsperaAtendimentoLogis = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> filaEsperaAtendimentoFrota = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> filaEsperaAtendimentoFinan = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> filaEsperaAtendimentoMobile = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> filaEsperaAtendimentoCloud = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> clientesEmAtendimento = Collections.synchronizedMap(new LinkedHashMap<String, Session>());

    //SALAS SUPORTE
    public static Map<String, Session> suportesOffline = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> suportesBanheiro = Collections.synchronizedMap(new LinkedHashMap<String, Session>());

    public static Map<String, Session> suportesDisponiveisTrans = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> suportesDisponiveisLogis = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> suportesDisponiveisFrota = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> suportesDisponiveisFinan = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> suportesDisponiveisMobile = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> suportesDisponiveisCloud = Collections.synchronizedMap(new LinkedHashMap<String, Session>());

    public static Map<String, Session> suportesEmAtendimento = Collections.synchronizedMap(new LinkedHashMap<String, Session>());

    //------
    @OnOpen
    public void onOpen(Session session) throws Exception {
        Map<String, List<String>> parametros = session.getRequestParameterMap();
        boolean isCliente = (parametros.get("is_cliente").get(0).equalsIgnoreCase("true"));

        //Caso seja cliente entra no if
        if (isCliente) {
            Cliente c = montarCliente(session);
            List<String> indexes = adicionarClienteFilaEspera(c, session);
            session.getBasicRemote().sendText(MSG_FILA_ESPERA + (indexes.indexOf(c.getNome()) + 1));
        } else {
            String login = parametros.get("login").get(0);
            String modulo = parametros.get("modulo").get(0);
            String status = parametros.get("status").get(0);
            String nome = parametros.get("nome").get(0);

            boolean existe = liberarSuporte(session, login, modulo);
            if (!existe) {
                if (login != null && nome != null && !login.equals("") && !nome.equals("")) {
                    session.getBasicRemote().sendText(Auxiliar.getHoraAtual() + "!@!" + "Gw Chat #@# Suporte, voc� est� conectado e seu status atual � " + status);
                    session.getUserProperties().put("nome", nome);
                    session.getUserProperties().put("login", login);
                    session.getUserProperties().put("modulo", modulo);
                    session.getUserProperties().put("status", status);

                    System.out.println("CHEGOU : " + status);
                    
                    if (status.equals("online")) {
                        if (modulo.equalsIgnoreCase("trans")) {
                            suportesDisponiveisTrans.put(login, session);
                        } else if (modulo.equalsIgnoreCase("logis")) {
                            suportesDisponiveisLogis.put(login, session);
                        } else if (modulo.equalsIgnoreCase("frota")) {
                            suportesDisponiveisFrota.put(login, session);
                        } else if (modulo.equalsIgnoreCase("finan")) {
                            suportesDisponiveisFinan.put(login, session);
                        } else if (modulo.equalsIgnoreCase("mobile")) {
                            suportesDisponiveisMobile.put(login, session);
                        } else if (modulo.equalsIgnoreCase("cloud")) {
                            suportesDisponiveisCloud.put(login, session);
                        }
                    } else if (status.equalsIgnoreCase("banheiro")) {
                        suportesBanheiro.put(login, session);
                    } else if (status.equalsIgnoreCase("offline")) {
                        suportesOffline.put(login, session);
                    }
                } else {
                    session.getBasicRemote().sendText(Auxiliar.getHoraAtual() + "!@!" + "Gw Chat #@# Suporte, n�o � possivel iniciar um atendimento sem nome ou login.");
                }
            }
        }

        gerenciarDeConexao(filaEsperaAtendimentoTrans, suportesDisponiveisTrans);
        gerenciarDeConexao(filaEsperaAtendimentoLogis, suportesDisponiveisLogis);
        gerenciarDeConexao(filaEsperaAtendimentoFrota, suportesDisponiveisFrota);
        gerenciarDeConexao(filaEsperaAtendimentoFinan, suportesDisponiveisFinan);
        gerenciarDeConexao(filaEsperaAtendimentoMobile, suportesDisponiveisMobile);
        gerenciarDeConexao(filaEsperaAtendimentoCloud, suportesDisponiveisCloud);
    }

    @OnMessage
    public void onMessage(Session session, String message) throws Exception {
        Map<String, List<String>> parametros = session.getRequestParameterMap();
        boolean isCliente = (parametros.get("is_cliente").get(0).equalsIgnoreCase("true"));
        if (isCliente) {
            String suporteAtendimento = String.valueOf(session.getUserProperties().get("suporteAtendimento"));
            Cliente cliente = montarCliente(session);
            if (!suporteAtendimento.equalsIgnoreCase("null")) {
                if (message.equals("VYZUALYS0U")) {
                    Session s = suportesEmAtendimento.get(suporteAtendimento);
                    s.getBasicRemote().sendText("VYZUALYS0U");
                } else {
                    Session s = suportesEmAtendimento.get(suporteAtendimento);
                    //Envia pro suporte
                    System.out.println("CHEGOU A MSG: " + message);
                    s.getBasicRemote().sendText(Auxiliar.getHoraAtual() + "!@!" + cliente.getNome().toUpperCase() + " #@# " + message);
                    //Envia pro cliente
                    session.getBasicRemote().sendText(Auxiliar.getHoraAtual() + "!@!" + cliente.getNome().toUpperCase() + " #@# " + message);
                }
            }
        } else {

            if (message.contains("ALTERAR_STATUS#!@!#")) {
                alterarStatusSuporte(session, message);

                gerenciarDeConexao(filaEsperaAtendimentoTrans, suportesDisponiveisTrans);
                gerenciarDeConexao(filaEsperaAtendimentoLogis, suportesDisponiveisLogis);
                gerenciarDeConexao(filaEsperaAtendimentoFrota, suportesDisponiveisFrota);
                gerenciarDeConexao(filaEsperaAtendimentoFinan, suportesDisponiveisFinan);
                gerenciarDeConexao(filaEsperaAtendimentoMobile, suportesDisponiveisMobile);
                gerenciarDeConexao(filaEsperaAtendimentoCloud, suportesDisponiveisCloud);
                
            } else {
                String clienteAtendimento = String.valueOf(session.getUserProperties().get("clienteAtendimento"));
                String suporte = String.valueOf(session.getUserProperties().get("nome"));
                if (!clienteAtendimento.equalsIgnoreCase("null")) {
                    if (message.equals("VYZUALYS0U")) {
                        Session s = clientesEmAtendimento.get(clienteAtendimento);
                        if (s != null) {
                            s.getBasicRemote().sendText("VYZUALYS0U");
                        }
                    } else {
                        Session s = clientesEmAtendimento.get(clienteAtendimento);
                        //Envia pro suporte
                        s.getBasicRemote().sendText(Auxiliar.getHoraAtual() + " !@! " + suporte.toUpperCase() + " #@# " + message);
                        //Envia pro cliente
                        session.getBasicRemote().sendText(Auxiliar.getHoraAtual() + " !@! " + suporte.toUpperCase() + " #@# " + message);
                    }
                }
            }
        }
    }

    @OnClose
    public void onClose(Session session) throws Exception {
        Map<String, List<String>> parametros = session.getRequestParameterMap();
        boolean isCliente = (parametros.get("is_cliente").get(0).equalsIgnoreCase("true"));
        if (isCliente) {
            clienteDesconectou(session);
        } else {
            suporteDesconectou(session);
        }
    }

    @OnError
    public void onError(Throwable t, Session s) throws Exception {
        s.getBasicRemote().sendText(t.getMessage());
    }

    public static void alterarStatusSuporte(Session session, String message) {
        String modulo = String.valueOf(session.getUserProperties().get("modulo"));
        String login = String.valueOf(session.getUserProperties().get("login"));

        if (modulo.equals("trans")) {
            if (suportesDisponiveisTrans.containsKey(login)) {
                suportesDisponiveisTrans.remove(login);
            }
        } else if (modulo.equalsIgnoreCase("logis")) {
            if (suportesDisponiveisLogis.containsKey(login)) {
                suportesDisponiveisLogis.remove(login);
            }
        } else if (modulo.equalsIgnoreCase("frota")) {
            if (suportesDisponiveisFrota.containsKey(login)) {
                suportesDisponiveisFrota.remove(login);
            }
        } else if (modulo.equalsIgnoreCase("finan")) {
            if (suportesDisponiveisFinan.containsKey(login)) {
                suportesDisponiveisFinan.remove(login);
            }
        } else if (modulo.equalsIgnoreCase("mobile")) {
            if (suportesDisponiveisMobile.containsKey(login)) {
                suportesDisponiveisMobile.remove(login);
            }
        } else if (modulo.equalsIgnoreCase("cloud")) {
            if (suportesDisponiveisCloud.containsKey(login)) {
                suportesDisponiveisCloud.remove(login);
            }
        }

        if (suportesBanheiro.containsKey(login)) {
            suportesBanheiro.remove(login);
        }

        if (suportesOffline.containsKey(login)) {
            suportesOffline.remove(login);
        }

        String status = message.split("#!@!#")[1];

        session.getUserProperties().remove("status");
        session.getUserProperties().put("status", status);

        if (status.equalsIgnoreCase("online")) {

            if (modulo.equals("trans")) {
                suportesDisponiveisTrans.put(login, session);
            } else if (modulo.equalsIgnoreCase("logis")) {
                suportesDisponiveisLogis.put(login, session);
            } else if (modulo.equalsIgnoreCase("frota")) {
                suportesDisponiveisFrota.put(login, session);
            } else if (modulo.equalsIgnoreCase("finan")) {
                suportesDisponiveisFinan.put(login, session);
            } else if (modulo.equalsIgnoreCase("mobile")) {
                suportesDisponiveisMobile.put(login, session);
            } else if (modulo.equalsIgnoreCase("cloud")) {
                suportesDisponiveisCloud.put(login, session);
            }

        } else if (status.equalsIgnoreCase("banheiro")) {
            suportesBanheiro.put(login, session);
        } else if (status.equalsIgnoreCase("offline")) {
            suportesOffline.put(login, session);
        }

    }

    public static Cliente montarCliente(Session session) {
        Cliente c;
        if (session.getUserProperties().get("nome_usuario") != null) {
            String nome_usuario = String.valueOf(session.getUserProperties().get("nome_usuario"));
            int id = Integer.parseInt(String.valueOf(session.getUserProperties().get("id")));
            String cnpj = String.valueOf(session.getUserProperties().get("cnpj"));
            String email = String.valueOf(session.getUserProperties().get("email"));
            String modulo = String.valueOf(session.getUserProperties().get("modulo"));
            c = new Cliente(nome_usuario, email, cnpj, modulo, id);
        } else {
            Map<String, List<String>> parametros = session.getRequestParameterMap();
            String nome_usuario = parametros.get("nome_usuario").get(0);
            session.getUserProperties().put("nome_usuario", nome_usuario);
            int id = Integer.parseInt(parametros.get("id").get(0));
            session.getUserProperties().put("id", id);
            String cnpj = parametros.get("cnpj").get(0);
            session.getUserProperties().put("cnpj", cnpj);
            String email = parametros.get("email").get(0);
            session.getUserProperties().put("email", email);
            String modulo = parametros.get("modulo").get(0);
            session.getUserProperties().put("modulo", modulo);

            c = new Cliente(nome_usuario, email, cnpj, modulo, id);
        }

        return c;
    }

    public List<String> adicionarClienteFilaEspera(Cliente cliente, Session session) throws Exception {
        String modulo = cliente.getModulo();
        List<String> indexes = null;
        int contador = 2;
        String nomeInicial = cliente.getNome();

        if (modulo.equals("trans")) {
            while (filaEsperaAtendimentoTrans.containsKey(cliente.getNome())) {
                cliente.setNome(nomeInicial + "_" + contador);
                //Remove o nome da session e adiciona o novo nome
                removerNomeAdicionarNomeSession(session, cliente.getNome());
                contador++;
            }
            filaEsperaAtendimentoTrans.put(cliente.getNome(), session);
            indexes = new ArrayList<>(filaEsperaAtendimentoTrans.keySet());
        } else if (modulo.equalsIgnoreCase("logis")) {
            while (filaEsperaAtendimentoLogis.containsKey(cliente.getNome())) {
                cliente.setNome(nomeInicial + "_" + contador);
                //Remove o nome da session e adiciona o novo nome
                removerNomeAdicionarNomeSession(session, cliente.getNome());
                contador++;
            }
            filaEsperaAtendimentoLogis.put(cliente.getNome(), session);
            indexes = new ArrayList<>(filaEsperaAtendimentoLogis.keySet());
        } else if (modulo.equalsIgnoreCase("frota")) {
            while (filaEsperaAtendimentoFrota.containsKey(cliente.getNome())) {
                cliente.setNome(nomeInicial + "_" + contador);
                //Remove o nome da session e adiciona o novo nome
                removerNomeAdicionarNomeSession(session, cliente.getNome());
                contador++;
            }
            filaEsperaAtendimentoFrota.put(cliente.getNome(), session);
            indexes = new ArrayList<>(filaEsperaAtendimentoFrota.keySet());
        } else if (modulo.equalsIgnoreCase("finan")) {
            while (filaEsperaAtendimentoFinan.containsKey(cliente.getNome())) {
                cliente.setNome(nomeInicial + "_" + contador);
                //Remove o nome da session e adiciona o novo nome
                removerNomeAdicionarNomeSession(session, cliente.getNome());
                contador++;
            }
            filaEsperaAtendimentoFinan.put(cliente.getNome(), session);
            indexes = new ArrayList<>(filaEsperaAtendimentoFinan.keySet());
        } else if (modulo.equalsIgnoreCase("mobile")) {
            while (filaEsperaAtendimentoMobile.containsKey(cliente.getNome())) {
                cliente.setNome(nomeInicial + "_" + contador);
                //Remove o nome da session e adiciona o novo nome
                removerNomeAdicionarNomeSession(session, cliente.getNome());
                contador++;
            }
            filaEsperaAtendimentoMobile.put(cliente.getNome(), session);
            indexes = new ArrayList<>(filaEsperaAtendimentoMobile.keySet());
        } else if (modulo.equalsIgnoreCase("cloud")) {
            while (filaEsperaAtendimentoCloud.containsKey(cliente.getNome())) {
                cliente.setNome(nomeInicial + "_" + contador);
                //Remove o nome da session e adiciona o novo nome
                removerNomeAdicionarNomeSession(session, cliente.getNome());
                contador++;
            }
            filaEsperaAtendimentoCloud.put(cliente.getNome(), session);
            indexes = new ArrayList<>(filaEsperaAtendimentoCloud.keySet());
        }

        if (contador != 2) {
            session.getBasicRemote().sendText(Auxiliar.getHoraAtual() + "!@!" + "Gw Chat #@#" + "J� existe um usu�rio na fila de espera com o mesmo nome, seu novo nome �: " + cliente.getNome());
        }

        return indexes;
    }

    public void removerNomeAdicionarNomeSession(Session session, String novoNome) throws Exception {
        List<String> nvNome = new ArrayList();
        nvNome.add(novoNome);
        try {
            session.getUserProperties().remove("nome_usuario");
            session.getUserProperties().put("nome_usuario", nvNome);
        } catch (Exception ex) {
            System.out.println("EXCEPTION : " + ex.getMessage());
        }
    }

    public static void gerenciarDeConexao(Map<String, Session> filaEsperaAtendimento, Map<String, Session> suportesDisponiveis) throws IOException {
        try {
            List<String> indexesSuporte = new ArrayList<>(suportesDisponiveis.keySet());

            if (!indexesSuporte.isEmpty()) {
                List<String> indexesClientes = new ArrayList<>(filaEsperaAtendimento.keySet());
                for (String proximoCliente : indexesClientes) {
                    //Pegando o cliente
                    Session sCliente = filaEsperaAtendimento.get(proximoCliente);
                    Cliente c = montarCliente(sCliente);

                    //Pegando o suporte
                    String proximoSuporte = indexesSuporte.get(0);
                    Session sSuporte = suportesDisponiveis.get(proximoSuporte);

                    //Adicionando o suporte no cliente
                    sCliente.getUserProperties().put("suporteAtendimento", proximoSuporte);
                    suportesDisponiveis.remove(proximoSuporte);
                    //Adicionando o suporte no cliente

                    Connection con = null;

                    try {
                        GerarConexao gCon = new GerarConexao();
                        con = gCon.novaConexao("jdbc:postgresql://localhost:5432/gwchat", "postgres", "postgres");
                        GwChatBO bo = new GwChatBO(con);

                        Ocorrencia ocorrencia = new Ocorrencia();
                        ocorrencia.setIdCliente(c.getIdCliente());
                        ocorrencia.setStatus_ocorrencia("P");
                        ocorrencia.setTipoCorrencia(1);
                        ocorrencia.setLoginSuporte(proximoSuporte);

                        String protocolo = bo.cadastrarOcorrencia(ocorrencia);
                        sSuporte.getUserProperties().put("clienteAtendimento", proximoCliente);
                        //remove da sala de fila para por na de atendimento
                        filaEsperaAtendimento.remove(proximoCliente);
                        //poe na sala de atendimento
                        clientesEmAtendimento.put(proximoCliente, sCliente);
                        suportesEmAtendimento.put(proximoSuporte, sSuporte);

                        sSuporte.getBasicRemote().sendText("INYCY0U@T3NDYM3NT0:"+protocolo);
                        sSuporte.getBasicRemote().sendText(MSG_SUPORTE_INICIO_ATENDIMENTO.replaceAll("<nmCliente>", proximoCliente).replaceAll("<nmSuporte>", String.valueOf(sSuporte.getUserProperties().get("nome"))).replaceAll("<N� Protocolo>", protocolo));
                        sCliente.getBasicRemote().sendText(MSG_SAUDACAO.replaceAll("<nmCliente>", proximoCliente).replaceAll("<nmSuporte>", String.valueOf(sSuporte.getUserProperties().get("nome"))).replaceAll("<N� Protocolo>", protocolo));

                        atualizarFilaEspera(filaEsperaAtendimento);
                    } finally {
                        if (con != null) {
                            con.close();
                        }
                    }
                }

            }

        } catch (Exception ex) {
            System.out.println("EX : " + ex);
            ex.printStackTrace();
        }
    }

    public static void atualizarFilaEspera(Map<String, Session> filaEsperaAtendimento) {
        try {
            if (!filaEsperaAtendimento.isEmpty()) {
                List<String> indexesClientes = new ArrayList<String>(filaEsperaAtendimento.keySet());
                for (String proximoCliente : indexesClientes) {
                    Session session = filaEsperaAtendimento.get(proximoCliente);
                    Cliente c = montarCliente(session);
                    session.getBasicRemote().sendText(MSG_GANHOU_POSICAO + (indexesClientes.indexOf(c.getNome()) + 1));
                }
            }

        } catch (IOException ex) {
            System.out.println("ERRO ATUALIZAR ESPERA : " + ex.getMessage());
        }
    }

    public void clienteDesconectou(Session session) throws Exception {
        Cliente usuarioLeft = montarCliente(session);
        String modulo = usuarioLeft.getModulo();

        String suporteAtendimento = String.valueOf(session.getUserProperties().get("suporteAtendimento"));
        Session sSuporte = null;
        if (!suporteAtendimento.equalsIgnoreCase("null")) {
            sSuporte = suportesEmAtendimento.get(suporteAtendimento);
            suportesEmAtendimento.remove(suporteAtendimento);
            //Envia pro suporte

            sSuporte.getBasicRemote().sendText(CLIENTE_DESCONECTOU.replace("<nmSuporte>", "Gw Chat"));
//            sSuporte.getBasicRemote().sendText(Auxiliar.getHoraAtual() + "!@!" + "Gw Chat #@# Voc� tem 15 segundos para alterar seu status.");
            sSuporte.getBasicRemote().sendText("D3SC0N3CT@R");
        }

        if (modulo.equals("trans")) {
            filaEsperaAtendimentoTrans.remove(usuarioLeft.getNome());
            if (sSuporte != null) {
                suportesDisponiveisTrans.put(suporteAtendimento, sSuporte);
            }
            atualizarFilaEspera(filaEsperaAtendimentoTrans);
        } else if (modulo.equalsIgnoreCase("logis")) {
            filaEsperaAtendimentoLogis.remove(usuarioLeft.getNome());
            if (sSuporte != null) {
                suportesDisponiveisLogis.put(suporteAtendimento, sSuporte);
            }
            atualizarFilaEspera(filaEsperaAtendimentoLogis);
        } else if (modulo.equalsIgnoreCase("frota")) {
            filaEsperaAtendimentoFrota.remove(usuarioLeft.getNome());
            if (sSuporte != null) {
                suportesDisponiveisFrota.put(suporteAtendimento, sSuporte);
            }
            atualizarFilaEspera(filaEsperaAtendimentoFrota);
        } else if (modulo.equalsIgnoreCase("finan")) {
            filaEsperaAtendimentoFinan.remove(usuarioLeft.getNome());
            if (sSuporte != null) {
                suportesDisponiveisFinan.put(suporteAtendimento, sSuporte);
            }
            atualizarFilaEspera(filaEsperaAtendimentoFinan);
        } else if (modulo.equalsIgnoreCase("mobile")) {
            filaEsperaAtendimentoMobile.remove(usuarioLeft.getNome());
            if (sSuporte != null) {
                suportesDisponiveisMobile.put(suporteAtendimento, sSuporte);
            }
            atualizarFilaEspera(filaEsperaAtendimentoMobile);
        } else if (modulo.equalsIgnoreCase("cloud")) {
            filaEsperaAtendimentoCloud.remove(usuarioLeft.getNome());
            if (sSuporte != null) {
                suportesDisponiveisCloud.put(suporteAtendimento, sSuporte);
            }
            atualizarFilaEspera(filaEsperaAtendimentoCloud);
        }

        if (clientesEmAtendimento.containsKey(usuarioLeft.getNome())) {
            clientesEmAtendimento.remove(usuarioLeft.getNome());
        }
    }

    public void suporteDesconectou(Session session) throws Exception {
        String login = String.valueOf(session.getUserProperties().get("login"));
        if (suportesEmAtendimento.containsKey(login)) {
            String loginCliente = String.valueOf(session.getUserProperties().get("clienteAtendimento"));
            if (loginCliente != null && !loginCliente.trim().equalsIgnoreCase("")) {
                Session sCliente = clientesEmAtendimento.get(loginCliente);
                sCliente.getBasicRemote().sendText(Auxiliar.getHoraAtual() + "!@!" + "Gw Chat #@#" + "O suporte desconectou da conversa, favor finalizar o atendimento.");
            }
            suportesEmAtendimento.remove(login);
        }
        if (suportesOffline.containsKey(login)) {
            suportesOffline.remove(login);
        }
        if (suportesBanheiro.containsKey(login)) {
            suportesBanheiro.remove(login);
        }
        if (suportesDisponiveisTrans.containsKey(login)) {
            suportesDisponiveisTrans.remove(login);
        }
        if (suportesDisponiveisCloud.containsKey(login)) {
            suportesDisponiveisCloud.remove(login);
        }
        if (suportesDisponiveisFinan.containsKey(login)) {
            suportesDisponiveisFinan.remove(login);
        }
        if (suportesDisponiveisFrota.containsKey(login)) {
            suportesDisponiveisFrota.remove(login);
        }
        if (suportesDisponiveisLogis.containsKey(login)) {
            suportesDisponiveisLogis.remove(login);
        }
        if (suportesDisponiveisMobile.containsKey(login)) {
            suportesDisponiveisMobile.remove(login);
        }
    }

    public boolean liberarSuporte(Session s, String login, String modulo) throws Exception {
        boolean existe = false;

        if (modulo.equalsIgnoreCase("trans")) {
            if (suportesDisponiveisTrans.containsKey(login)) {
                existe = true;
            }
        } else if (modulo.equalsIgnoreCase("logis")) {
            if (suportesDisponiveisLogis.containsKey(login)) {
                existe = true;
            }
        } else if (modulo.equalsIgnoreCase("frota")) {
            if (suportesDisponiveisFrota.containsKey(login)) {
                existe = true;
            }
        } else if (modulo.equalsIgnoreCase("finan")) {
            if (suportesDisponiveisFinan.containsKey(login)) {
                existe = true;
            }
        } else if (modulo.equalsIgnoreCase("mobile")) {
            if (suportesDisponiveisMobile.containsKey(login)) {
                existe = true;
            }
        } else if (modulo.equalsIgnoreCase("cloud")) {
            if (suportesDisponiveisCloud.containsKey(login)) {
                existe = true;
            }
        }

        if (suportesOffline.containsKey(login)) {
            existe = true;
        }
        if (suportesBanheiro.containsKey(login)) {
            existe = true;
        }
        if (suportesEmAtendimento.containsKey(login)) {
            existe = true;
        }

        if (existe) {
            s.getBasicRemote().sendText(Auxiliar.getHoraAtual() + "!@!" + "Gw Chat #@#" + "J� existe um suporte com esses dados utlizando o sistema.");
        }

        return existe;

    }
}
