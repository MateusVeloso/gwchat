package br.com.gwchat.controladores;

import br.com.gwchat.apoio.Auxiliar;
import br.com.gwchat.apoio.GerarConexao;
import br.com.gwchat.bo.GwChatBO;
import br.com.gwchat.geral.Cliente;
import br.com.gwchat.geral.Ocorrencia;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author Mateus
 */
@ServerEndpoint(value = "/chatServerSuporte")
public class ChatServerSuporte {

    final static String MSG_SAUDACAO = Auxiliar.getHoraAtual() + "!@!<nmSuporte>#@#Bom dia Sr(a) <nmCliente>, eu sou <nmSuporte> e o protocolo desse atendimento � <N� Protocolo>, em que posso ajudar?";
    final static String MSG_SUPORTE_INICIO_ATENDIMENTO = Auxiliar.getHoraAtual()+"!@!<nmSuporte>#@#Atendimento iniciado com o cliente <nmClienteBase> <br> Contato: <nmCliente> <br>  Protocolo: <N� Protocolo>.";
    final static String CLIENTE_DESCONECTOU = Auxiliar.getHoraAtual()+"!@!<nmSuporte>#@#Cliente desconectou da conversa";

    final static String NOME_SUPORTE = "nome_suporte";
    final static String USER = "nome_usuario";

    public static Map<String, Session> suportesOffline = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> suportesBanheiro = Collections.synchronizedMap(new LinkedHashMap<String, Session>());

    public static Map<String, Session> suportesDisponiveisTrans = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> suportesDisponiveisLogis = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> suportesDisponiveisFrota = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> suportesDisponiveisFinan = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> suportesDisponiveisMobile = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> suportesDisponiveisCloud = Collections.synchronizedMap(new LinkedHashMap<String, Session>());

    public static Map<String, Session> suportesEmAtendimento = Collections.synchronizedMap(new LinkedHashMap<String, Session>());

    @OnOpen
    public void onOpen(Session session) throws Exception {

        Map<String, List<String>> parametros = session.getRequestParameterMap();
        
        List<String> listLogin = parametros.get("login");
        List<String> listModulo = parametros.get("modulo");
        List<String> status = parametros.get("status");
        
        String login = listLogin.get(0);
        String modulo = listModulo.get(0);

        if (!login.equalsIgnoreCase("")) {
            session.getBasicRemote().sendText(Auxiliar.getHoraAtual() + "!@!" + "Gw Chat #@# Suporte, voc� est� conectado e seu status atual � "+status.get(0));
            session.getUserProperties().put(NOME_SUPORTE, login);
            session.getUserProperties().put("modulo", modulo);
            
            System.out.println("CHEGOU status : " + modulo);
            if (status.get(0).equals("online")) {
                
                System.out.println("CHEGOU modulo : " + modulo);
                
                if (modulo.equalsIgnoreCase("trans")) {
                    suportesDisponiveisTrans.put(login, session);
                }else if (modulo.equalsIgnoreCase("logis")) {
                    suportesDisponiveisLogis.put(login, session);
                }else if (modulo.equalsIgnoreCase("frota")) {
                    suportesDisponiveisFrota.put(login, session);
                }else if (modulo.equalsIgnoreCase("finan")) {
                    suportesDisponiveisFinan.put(login, session);
                }else if (modulo.equalsIgnoreCase("mobile")) {
                    suportesDisponiveisMobile.put(login, session);
                }else if (modulo.equalsIgnoreCase("cloud")) {
                    suportesDisponiveisCloud.put(login, session);
                }
            }else{
                suportesOffline.put(login, session);
            }

            System.out.println("SUPORTES OFF : " + suportesOffline.size());
            gerenciarDeConexao(ChatServerCliente.filaEsperaAtendimentoTrans, suportesDisponiveisTrans);
            gerenciarDeConexao(ChatServerCliente.filaEsperaAtendimentoLogis, suportesDisponiveisLogis);
            gerenciarDeConexao(ChatServerCliente.filaEsperaAtendimentoFrota, suportesDisponiveisFrota);
            gerenciarDeConexao(ChatServerCliente.filaEsperaAtendimentoFinan, suportesDisponiveisFinan);
            gerenciarDeConexao(ChatServerCliente.filaEsperaAtendimentoMobile, suportesDisponiveisMobile);
            gerenciarDeConexao(ChatServerCliente.filaEsperaAtendimentoCloud, suportesDisponiveisCloud);
        } else {
            session.getBasicRemote().sendText(Auxiliar.getHoraAtual() + "!@!" + "Gw Chat #@# Suporte, voc� n�o � um suporte valido.");
        }

    }
    

    @OnMessage
    public void onMessage(Session session, String message) throws Exception {
        String clienteAtendimento = String.valueOf(session.getUserProperties().get("clienteAtendimento"));
        String suporte = String.valueOf(session.getUserProperties().get(NOME_SUPORTE));
        if (!clienteAtendimento.equalsIgnoreCase("null")) {
            if (message.equals("VYZUALYS0U")) {
                Session s = ChatServerCliente.clientesEmAtendimento.get(clienteAtendimento);
                if (s != null) {
                    s.getBasicRemote().sendText("VYZUALYS0U");
                }
            } else {
                Session s = ChatServerCliente.clientesEmAtendimento.get(clienteAtendimento);
                //Envia pro suporte
                s.getBasicRemote().sendText(Auxiliar.getHoraAtual() + " !@! " + suporte.toUpperCase() + " #@# " + message);
                //Envia pro cliente
                session.getBasicRemote().sendText(Auxiliar.getHoraAtual() + " !@! " + suporte.toUpperCase() + " #@# " + message);
            }
        }
    }

    @OnClose
    public void onClose(Session session) throws Exception {
        System.out.println("CHEGOU ONCLOSE");
    }

    public static void gerenciarDeConexao(Map<String, Session> filaEsperaAtendimento, Map<String, Session> suportesDisponiveis) throws IOException {
        boolean continuar = true;
        try {
            List<String> indexesClientes = new ArrayList<String>(filaEsperaAtendimento.keySet());
            for (String proximoCliente : indexesClientes) {
                List<String> indexesSuporte = new ArrayList<String>(suportesDisponiveis.keySet());
                if (indexesSuporte.size() > 0) {
                    Session sCliente = filaEsperaAtendimento.get(proximoCliente);
                    Cliente c = (Cliente) sCliente.getUserProperties().get(USER);
                    String proximoSuporte = indexesSuporte.get(0);

                    Session sSuporte = suportesDisponiveis.get(proximoSuporte);
                    sCliente.getUserProperties().put("suporteAtendimento", proximoSuporte);
                    suportesDisponiveis.remove(proximoSuporte);

                    
                    if (sSuporte != null) {
                        Connection con = null;
                        try {
                            GerarConexao gCon = new GerarConexao();
                            con = gCon.novaConexao("jdbc:postgresql://192.168.0.252:5433/MATEUS_INOVACAO_HOMEM", "postgres", "postgres");
                            GwChatBO bo = new GwChatBO(con);

                            Ocorrencia ocorrencia = new Ocorrencia();
                            ocorrencia.setIdCliente(c.getIdCliente());
                            ocorrencia.setStatus_ocorrencia("P");
                            ocorrencia.setTipoCorrencia(1);
                            ocorrencia.setLoginSuporte(proximoSuporte);

                            String protocolo = bo.cadastrarOcorrencia(ocorrencia);

                            sSuporte.getUserProperties().put("clienteAtendimento", proximoCliente);
                            //remove da sala de fila para por na de atendimento
                            filaEsperaAtendimento.remove(proximoCliente);
                            //poe na sala de atendimento
                            ChatServerCliente.clientesEmAtendimento.put(proximoCliente, sCliente);
                            ChatServerSuporte.suportesEmAtendimento.put(proximoSuporte, sSuporte);
                            sSuporte.getBasicRemote().sendText(MSG_SUPORTE_INICIO_ATENDIMENTO.replaceAll("<nmCliente>", proximoCliente).replaceAll("<nmSuporte>", proximoSuporte).replaceAll("<N� Protocolo>",protocolo));
                            sCliente.getBasicRemote().sendText(MSG_SAUDACAO.replaceAll("<nmCliente>", proximoCliente).replaceAll("<nmSuporte>", proximoSuporte).replaceAll("<N� Protocolo>", protocolo));
                            ChatServerCliente.atualizarFilaEspera(filaEsperaAtendimento);
                        } finally {
                            if (con != null) {
                                con.close();
                            }
                        }
                    }
                }
            }

        } catch (Exception ex) {
            System.out.println("EX : " + ex);
        }
    }
}
