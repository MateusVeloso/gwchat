package br.com.gwchat.controladores;

import br.com.gwchat.apoio.GerarConexao;
import br.com.gwchat.bo.GwChatBO;
import br.com.gwchat.geral.Cliente;
import br.com.gwchat.geral.Suporte;
import br.com.gwchat.geral.SuporteBO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.Session;

/**
 *
 * @author Mateus
 */
public class GwChatControlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    PrintWriter out = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=ISO-8859-1");

        String acao = request.getParameter("acao");
        out = response.getWriter();

        try {
            if (acao.equalsIgnoreCase("logar_suporte")) {
                if (this.logarSuporte(request, response)) {
                    RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
                    dispatcher.forward(request, response);
                } else {
                    RequestDispatcher dispatcher = request.getRequestDispatcher("login-suporte");
                    request.setAttribute("erro", "Suporte n�o encontrado");
                    dispatcher.forward(request, response);
                }
            } else if (acao.equalsIgnoreCase("existeCnpjSerial")) {
                out.print(this.existeCnpjSerial(request, response));
            } else if (acao.equalsIgnoreCase("iniciar_chat")) {
                Cliente c = new Cliente(request.getParameter("nome"), request.getParameter("email"), request.getParameter("cnpjSerial"), request.getParameter("modulo"), Integer.parseInt(request.getParameter("idCliente")));
                request.setAttribute("cliente", c);
                RequestDispatcher dispatcher = request.getRequestDispatcher("cliente");
                dispatcher.forward(request, response);
            } else if (acao.equalsIgnoreCase("logout_suporte")) {
                request.getSession(false).removeAttribute("suporte");
                out.println(1);
            } else if (acao.equalsIgnoreCase("alterarStatusSuporte")) {
                String modulo = request.getParameter("modulo");
                String status = request.getParameter("status");
                Suporte suporte = (Suporte) request.getSession(false).getAttribute("suporte");

                status = status.toLowerCase();

                Session s = null;
                if (modulo.equals("trans")) {
                    s = removerSuporteChannel(suporte);
                    if (status.equalsIgnoreCase("banheiro")) {
                        suporte.setStatus("banheiro");
                        alterarSuporteSession(request, suporte);
                        ChatServer.suportesBanheiro.put(suporte.getLogin(), s);
                    } else if (status.equalsIgnoreCase("offline")) {
                        suporte.setStatus("offline");
                        alterarSuporteSession(request, suporte);
                        ChatServer.suportesOffline.put(suporte.getLogin(), s);
                    } else if (status.equalsIgnoreCase("online")) {
                        suporte.setStatus("online");
                        alterarSuporteSession(request, suporte);
                        ChatServer.suportesDisponiveisTrans.put(suporte.getLogin(), s);
                    }
                    ChatServer.gerenciarDeConexao(ChatServer.filaEsperaAtendimentoTrans, ChatServer.suportesDisponiveisTrans);

                } else if (modulo.equalsIgnoreCase("logis")) {
                    s = removerSuporteChannel(suporte);
                    if (status.equalsIgnoreCase("banheiro")) {
                        ChatServer.suportesBanheiro.put(suporte.getLogin(), s);
                    } else if (status.equalsIgnoreCase("offline")) {
                        ChatServer.suportesOffline.put(suporte.getLogin(), s);
                    } else if (status.equalsIgnoreCase("online")) {
                        ChatServer.suportesDisponiveisLogis.put(suporte.getLogin(), s);
                    }

                    ChatServer.gerenciarDeConexao(ChatServer.filaEsperaAtendimentoLogis, ChatServer.suportesDisponiveisLogis);

                } else if (modulo.equalsIgnoreCase("frota")) {
                    s = removerSuporteChannel(suporte);
                    if (status.equalsIgnoreCase("banheiro")) {
                        ChatServer.suportesBanheiro.put(suporte.getLogin(), s);
                    } else if (status.equalsIgnoreCase("offline")) {
                        ChatServer.suportesOffline.put(suporte.getLogin(), s);
                    } else if (status.equalsIgnoreCase("online")) {
                        ChatServer.suportesDisponiveisFrota.put(suporte.getLogin(), s);
                    }

                    ChatServer.gerenciarDeConexao(ChatServer.filaEsperaAtendimentoFrota, ChatServer.suportesDisponiveisFrota);

                } else if (modulo.equalsIgnoreCase("finan")) {
                    s = removerSuporteChannel(suporte);
                    if (status.equalsIgnoreCase("banheiro")) {
                        ChatServer.suportesBanheiro.put(suporte.getLogin(), s);
                    } else if (status.equalsIgnoreCase("offline")) {
                        ChatServer.suportesOffline.put(suporte.getLogin(), s);
                    } else if (status.equalsIgnoreCase("online")) {
                        ChatServer.suportesDisponiveisFinan.put(suporte.getLogin(), s);
                    }

                    ChatServer.gerenciarDeConexao(ChatServer.filaEsperaAtendimentoFinan, ChatServer.suportesDisponiveisFinan);

                } else if (modulo.equalsIgnoreCase("mobile")) {
                    s = removerSuporteChannel(suporte);
                    if (status.equalsIgnoreCase("banheiro")) {
                        ChatServer.suportesBanheiro.put(suporte.getLogin(), s);
                    } else if (status.equalsIgnoreCase("offline")) {
                        ChatServer.suportesOffline.put(suporte.getLogin(), s);
                    } else if (status.equalsIgnoreCase("online")) {
                        ChatServer.suportesDisponiveisMobile.put(suporte.getLogin(), s);
                    }

                    ChatServer.gerenciarDeConexao(ChatServer.filaEsperaAtendimentoMobile, ChatServer.suportesDisponiveisMobile);

                } else if (modulo.equalsIgnoreCase("cloud")) {
                    s = removerSuporteChannel(suporte);
                    if (status.equalsIgnoreCase("banheiro")) {
                        ChatServer.suportesBanheiro.put(suporte.getLogin(), s);
                    } else if (status.equalsIgnoreCase("offline")) {
                        ChatServer.suportesOffline.put(suporte.getLogin(), s);
                    } else if (status.equalsIgnoreCase("online")) {
                        ChatServer.suportesDisponiveisCloud.put(suporte.getLogin(), s);
                    }
                    ChatServer.gerenciarDeConexao(ChatServer.filaEsperaAtendimentoTrans, ChatServer.suportesDisponiveisCloud);

                }

            } else if (acao.equalsIgnoreCase("gravarMotivoOcorrencia")) {
                this.gravarMotivoOcorrencia(request, response);
            } else if (acao.equalsIgnoreCase("gravarConversa")){
                this.gravarConversa(request,response);
            }
        } catch (Exception ex) {
            System.out.println("EX CHAT CONTROLADOR : " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    private String existeCnpjSerial(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        int idCliente = 0;
        String cnpjSerial = request.getParameter("cnpjSerial");
        Connection con = null;
        try {
            con = new GerarConexao().novaConexao(request.getServletContext().getInitParameter("base"), request.getServletContext().getInitParameter("usuarioBanco"), request.getServletContext().getInitParameter("senhaBanco"));
            GwChatBO bo = new GwChatBO(con);
            idCliente = bo.existeCnpjSerial(cnpjSerial);

        } finally {
            if (con != null && !con.isClosed()) {
                con.close();
            }
        }

        if (idCliente != 0) {
            return "cliente encontrado!@!" + idCliente;
        } else if (cnpjSerial.length() > 8) {
            return "CNPJ n�o Encontrado!@!0";
        } else {
            return "Serial n�o encontrado!@!0";
        }

    }

    private boolean logarSuporte(HttpServletRequest request, HttpServletResponse response) throws SQLException, Exception {
        boolean retorno = false;
        String login = request.getParameter("login");
        String senha = request.getParameter("senha");
        SuporteBO bo;
        Connection con = null;

        if (isNotEmpity(login) && isNotEmpity(senha)) {
            try {
                con = new GerarConexao().novaConexao(request.getServletContext().getInitParameter("base"), request.getServletContext().getInitParameter("usuarioBanco"), request.getServletContext().getInitParameter("senhaBanco"));
                bo = new SuporteBO(con);

                Suporte suporte = bo.logarSuporte(login, senha);
                if (suporte != null) {
                    retorno = true;
                    request.getSession(true).setAttribute("suporte", suporte);
                    for (String modulo : suporte.getModulos()) {
                        Collection<String> listaMotivos = carregarMotivosOcorrencia(request, response, modulo);
                        request.setAttribute("listaMotivos", listaMotivos);
                    }
                }
            } finally {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
            }

        }
        return retorno;
    }

    private boolean isNotEmpity(String campo) {
        if (campo != null && !campo.trim().equals("")) {
            return true;
        } else {
            return false;
        }
    }

    public static void alterarSuporteSession(HttpServletRequest r, Suporte suporte) {
        r.getSession(false).removeAttribute("suporte");
        r.getSession(false).setAttribute("suporte", suporte);
    }

    public static Session removerSuporteChannel(Suporte suporte) {
        Session s = null;
        if (suporte.getStatus().equalsIgnoreCase("offline")) {
            s = ChatServer.suportesOffline.remove(suporte.getLogin());
        } else if (suporte.getStatus().equalsIgnoreCase("banheiro")) {
            s = ChatServer.suportesBanheiro.remove(suporte.getLogin());
        } else {
            for (String modulo : suporte.getModulos()) {
                if (modulo.equalsIgnoreCase("trans")) {
                    s = ChatServer.suportesDisponiveisTrans.remove(suporte.getLogin());
                } else if (modulo.equalsIgnoreCase("finan")) {
                    s = ChatServer.suportesDisponiveisFinan.remove(suporte.getLogin());
                } else if (modulo.equalsIgnoreCase("frota")) {
                    s = ChatServer.suportesDisponiveisFrota.remove(suporte.getLogin());
                } else if (modulo.equalsIgnoreCase("logis")) {
                    s = ChatServer.suportesDisponiveisLogis.remove(suporte.getLogin());
                } else if (modulo.equalsIgnoreCase("cloud")) {
                    s = ChatServer.suportesDisponiveisCloud.remove(suporte.getLogin());
                } else if (modulo.equalsIgnoreCase("mobile")) {
                    s = ChatServer.suportesDisponiveisMobile.remove(suporte.getLogin());
                }
            }
        }
        return s;
    }

    private Collection<String> carregarMotivosOcorrencia(HttpServletRequest request, HttpServletResponse response, String sistema) throws Exception {
        Connection con = new GerarConexao().novaConexao(request.getServletContext().getInitParameter("base"), request.getServletContext().getInitParameter("usuarioBanco"), request.getServletContext().getInitParameter("senhaBanco"));
        GwChatBO bo = new GwChatBO(con);
        Collection<String> listaMotivo = bo.carregarMotivosOcorrencia(sistema);
        return listaMotivo;
    }

    private void gravarMotivoOcorrencia(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Connection con = new GerarConexao().novaConexao(request.getServletContext().getInitParameter("base"), request.getServletContext().getInitParameter("usuarioBanco"), request.getServletContext().getInitParameter("senhaBanco"));
        GwChatBO bo = new GwChatBO(con);
        
        String motivo = request.getParameter("motivo");
        String observacao = request.getParameter("observacao");
        String protocolo = request.getParameter("protocolo");
        
        try {
            bo.gravarMotivoOcorrencia(motivo, observacao, protocolo);
        } finally {
            if (con != null && !con.isClosed()) {
                con.close();
            }
        }
    }
    
    private void gravarConversa(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Connection con = new GerarConexao().novaConexao(request.getServletContext().getInitParameter("base"), request.getServletContext().getInitParameter("usuarioBanco"), request.getServletContext().getInitParameter("senhaBanco"));
        GwChatBO bo = new GwChatBO(con);
        
        String conversa = request.getParameter("conversa");
        String protocolo = request.getParameter("protocolo");
        
        System.out.println("CHEGOU : " + conversa);
        
        try {
            bo.gravarConversa(conversa, protocolo);
        } finally {
            if (con != null && !con.isClosed()) {
                con.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
