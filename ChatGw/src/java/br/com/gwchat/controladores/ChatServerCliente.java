package br.com.gwchat.controladores;

import br.com.gwchat.apoio.Auxiliar;
import br.com.gwchat.apoio.GerarConexao;
import br.com.gwchat.bo.GwChatBO;
import static br.com.gwchat.controladores.ChatServerSuporte.MSG_SAUDACAO;
import br.com.gwchat.geral.Cliente;
import br.com.gwchat.geral.Ocorrencia;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author Mateus
 */
@ServerEndpoint(value = "/chatServerCliente")
public class ChatServerCliente {

    final static String MSG_FILA_ESPERA = Auxiliar.getHoraAtual() + "!@!" + "Gw Chat #@#" + "Seu atendimento foi registrado, favor aguardar. Sua posi��o �: ";
    final static String MSG_GANHOU_POSICAO = Auxiliar.getHoraAtual() + "!@!" + "Gw Chat #@#" + "Sua posi��o agora �:";
    final static String MSG_SAUDACAO = Auxiliar.getHoraAtual() + "!@!<nmSuporte>#@#Bom dia Sr(a) <nmCliente>, eu sou <nmSuporte> e o protocolo desse atendimento � <N� Protocolo>, em que posso ajudar?";

    final static String MSG_SUPORTE_INICIO_ATENDIMENTO = Auxiliar.getHoraAtual()+"!@!<nmSuporte>#@#Atendimento iniciado com o cliente <nmClienteBase> <br> Contato: <nmCliente> <br>  Protocolo: <N� Protocolo>.";
    final static String CLIENTE_DESCONECTOU = Auxiliar.getHoraAtual()+"!@!<nmSuporte>#@#Cliente desconectou da conversa";

    final static String USER = "nome_usuario";
//    static Map<String, Session> filaEsperaAtendimento = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> filaEsperaAtendimentoTrans = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> filaEsperaAtendimentoLogis = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> filaEsperaAtendimentoFrota = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> filaEsperaAtendimentoFinan = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> filaEsperaAtendimentoMobile = Collections.synchronizedMap(new LinkedHashMap<String, Session>());
    public static Map<String, Session> filaEsperaAtendimentoCloud = Collections.synchronizedMap(new LinkedHashMap<String, Session>());

    static Map<String, Session> clientesEmAtendimento = Collections.synchronizedMap(new LinkedHashMap<String, Session>());

    @OnOpen
    public void onOpen(Session session) throws Exception {
        System.out.println("MODULO :  " + session);
        Cliente c = montarCliente(session);

        String modulo = c.getModulo();

        System.out.println("MODULO :  " + modulo);
        if (modulo.equals("trans")) {
            if (filaEsperaAtendimentoTrans.containsKey(c.getNome())) {
                throw new Exception(Auxiliar.getHoraAtual() + "!@!Gw Error#@#J� existe um cliente na fila de espera com esse login, o chat ir�");
            }
            // Cliente inserido na fila de espera
            filaEsperaAtendimentoTrans.put(c.getNome(), session);
            session.getUserProperties().put(USER, c);
            List<String> indexes = new ArrayList<String>(filaEsperaAtendimentoTrans.keySet());
            session.getBasicRemote().sendText(MSG_FILA_ESPERA + (indexes.indexOf(c.getNome()) + 1));
            this.gerenciarDeConexao(ChatServerCliente.filaEsperaAtendimentoTrans, ChatServerSuporte.suportesDisponiveisTrans);
        } else if (modulo.equalsIgnoreCase("logis")) {

            if (filaEsperaAtendimentoLogis.containsKey(c.getNome())) {
                throw new Exception("J� existe um cliente na fila de espera com esse login.");
            }
            // Cliente inserido na fila de espera
            filaEsperaAtendimentoLogis.put(c.getNome(), session);
            session.getUserProperties().put(USER, c);
            List<String> indexes = new ArrayList<String>(filaEsperaAtendimentoLogis.keySet());
            session.getBasicRemote().sendText(MSG_FILA_ESPERA + (indexes.indexOf(c.getNome()) + 1));
            this.gerenciarDeConexao(ChatServerCliente.filaEsperaAtendimentoLogis, ChatServerSuporte.suportesDisponiveisLogis);
        } else if (modulo.equalsIgnoreCase("frota")) {
            if (filaEsperaAtendimentoFrota.containsKey(c.getNome())) {
                throw new Exception("J� existe um cliente na fila de espera com esse login.");
            }
            // Cliente inserido na fila de espera
            filaEsperaAtendimentoFrota.put(c.getNome(), session);
            session.getUserProperties().put(USER, c);
            List<String> indexes = new ArrayList<String>(filaEsperaAtendimentoFrota.keySet());
            session.getBasicRemote().sendText(MSG_FILA_ESPERA + (indexes.indexOf(c.getNome()) + 1));
            this.gerenciarDeConexao(ChatServerCliente.filaEsperaAtendimentoFrota, ChatServerSuporte.suportesDisponiveisFrota);
        } else if (modulo.equalsIgnoreCase("finan")) {
            if (filaEsperaAtendimentoFinan.containsKey(c.getNome())) {
                throw new Exception("J� existe um cliente na fila de espera com esse login.");
            }
            // Cliente inserido na fila de espera
            filaEsperaAtendimentoFinan.put(c.getNome(), session);
            session.getUserProperties().put(USER, c);
            List<String> indexes = new ArrayList<String>(filaEsperaAtendimentoFinan.keySet());
            session.getBasicRemote().sendText(MSG_FILA_ESPERA + (indexes.indexOf(c.getNome()) + 1));
            this.gerenciarDeConexao(ChatServerCliente.filaEsperaAtendimentoFinan, ChatServerSuporte.suportesDisponiveisFinan);
        } else if (modulo.equalsIgnoreCase("mobile")) {
            if (filaEsperaAtendimentoMobile.containsKey(c.getNome())) {
                throw new Exception("J� existe um cliente na fila de espera com esse login.");
            }
            // Cliente inserido na fila de espera
            filaEsperaAtendimentoMobile.put(c.getNome(), session);
            session.getUserProperties().put(USER, c);
            List<String> indexes = new ArrayList<String>(filaEsperaAtendimentoMobile.keySet());
            session.getBasicRemote().sendText(MSG_FILA_ESPERA + (indexes.indexOf(c.getNome()) + 1));
            this.gerenciarDeConexao(ChatServerCliente.filaEsperaAtendimentoMobile, ChatServerSuporte.suportesDisponiveisMobile);
        } else if (modulo.equalsIgnoreCase("cloud")) {
            if (filaEsperaAtendimentoCloud.containsKey(c.getNome())) {
                throw new Exception("J� existe um cliente na fila de espera com esse login.");
            }
            // Cliente inserido na fila de espera
            filaEsperaAtendimentoCloud.put(c.getNome(), session);
            session.getUserProperties().put(USER, c);
            List<String> indexes = new ArrayList<String>(filaEsperaAtendimentoCloud.keySet());
            session.getBasicRemote().sendText(MSG_FILA_ESPERA + (indexes.indexOf(c.getNome()) + 1));
            this.gerenciarDeConexao(ChatServerCliente.filaEsperaAtendimentoCloud, ChatServerSuporte.suportesDisponiveisCloud);
        }
    }

    @OnMessage
    public void onMessage(Session session, String message) throws Exception {
        String suporteAtendimento = String.valueOf(session.getUserProperties().get("suporteAtendimento"));
        Cliente cliente = (Cliente) session.getUserProperties().get("nome_usuario");
        if (!suporteAtendimento.equalsIgnoreCase("null")) {
            if (message.equals("VYZUALYS0U")) {
                Session s = ChatServerSuporte.suportesEmAtendimento.get(suporteAtendimento);
                s.getBasicRemote().sendText("VYZUALYS0U");
            } else {
                Session s = ChatServerSuporte.suportesEmAtendimento.get(suporteAtendimento);
                //Envia pro suporte
                s.getBasicRemote().sendText(Auxiliar.getHoraAtual() + "!@!" + cliente.getNome().toUpperCase() + " #@# " + message);
                //Envia pro cliente
                session.getBasicRemote().sendText(Auxiliar.getHoraAtual() + "!@!" + cliente.getNome().toUpperCase() + " #@# " + message);
            }
        }
    }

    @OnClose
    public void onClose(Session session) throws Exception {
        Cliente usuarioLeft = (Cliente) session.getUserProperties().get(USER);
        String modulo = usuarioLeft.getModulo();

        String suporteAtendimento = String.valueOf(session.getUserProperties().get("suporteAtendimento"));
        Session sSuporte = null;
        if (!suporteAtendimento.equalsIgnoreCase("null")) {
            sSuporte = ChatServerSuporte.suportesEmAtendimento.get(suporteAtendimento);
            //Envia pro suporte
            sSuporte.getBasicRemote().sendText(CLIENTE_DESCONECTOU.replace("<nmSuporte>", "Gw Chat"));
            
            
            sSuporte.getBasicRemote().sendText(Auxiliar.getHoraAtual() + "!@!" + "Gw Chat #@# Voc� tem 15 segundos para alterar seu status.");
            ChatServerSuporte.suportesEmAtendimento.remove(suporteAtendimento);
            sSuporte.getBasicRemote().sendText("D3SC0N3CT@R");
        }

        if (modulo.equals("trans")) {
            filaEsperaAtendimentoTrans.remove(usuarioLeft.getNome());
            if (sSuporte != null) {
                ChatServerSuporte.suportesDisponiveisTrans.put(suporteAtendimento, sSuporte);
            }
            atualizarFilaEspera(filaEsperaAtendimentoTrans);
        } else if (modulo.equalsIgnoreCase("logis")) {
            filaEsperaAtendimentoLogis.remove(usuarioLeft.getNome());
            if (sSuporte != null) {
                ChatServerSuporte.suportesDisponiveisLogis.put(suporteAtendimento, sSuporte);
            }
            atualizarFilaEspera(filaEsperaAtendimentoLogis);
        } else if (modulo.equalsIgnoreCase("frota")) {
            filaEsperaAtendimentoFrota.remove(usuarioLeft.getNome());
            if (sSuporte != null) {
                ChatServerSuporte.suportesDisponiveisFrota.put(suporteAtendimento, sSuporte);
            }
            atualizarFilaEspera(filaEsperaAtendimentoFrota);
        } else if (modulo.equalsIgnoreCase("finan")) {
            filaEsperaAtendimentoFinan.remove(usuarioLeft.getNome());
            if (sSuporte != null) {
                ChatServerSuporte.suportesDisponiveisFinan.put(suporteAtendimento, sSuporte);
            }
            atualizarFilaEspera(filaEsperaAtendimentoFinan);
        } else if (modulo.equalsIgnoreCase("mobile")) {
            filaEsperaAtendimentoMobile.remove(usuarioLeft.getNome());
            if (sSuporte != null) {
                ChatServerSuporte.suportesDisponiveisMobile.put(suporteAtendimento, sSuporte);
            }
            atualizarFilaEspera(filaEsperaAtendimentoMobile);
        } else if (modulo.equalsIgnoreCase("cloud")) {
            filaEsperaAtendimentoCloud.remove(usuarioLeft.getNome());
            if (sSuporte != null) {
                ChatServerSuporte.suportesDisponiveisCloud.put(suporteAtendimento, sSuporte);
            }
            atualizarFilaEspera(filaEsperaAtendimentoCloud);
        }

        if (clientesEmAtendimento.containsKey(usuarioLeft.getNome())) {
            clientesEmAtendimento.remove(usuarioLeft.getNome());
        }

    }

    @OnError
    public void onError(Throwable t, Session e) throws IOException {
        e.getBasicRemote().sendText(t.getMessage());
    }

    public void gerenciarDeConexao(Map<String, Session> filaEsperaAtendimento, Map<String, Session> suportesDisponiveis) throws IOException {
        boolean continuar = true;
        try {
            List<String> indexesClientes = new ArrayList<String>(filaEsperaAtendimento.keySet());
            for (String proximoCliente : indexesClientes) {
                Session sCliente = filaEsperaAtendimento.get(proximoCliente);
                Cliente c = (Cliente) sCliente.getUserProperties().get(USER);
                List<String> indexesSuporte = new ArrayList<String>(suportesDisponiveis.keySet());
                if (indexesSuporte.size() > 0) {
                    String proximoSuporte = indexesSuporte.get(0);

                    Session sSuporte = suportesDisponiveis.get(proximoSuporte);
                    sCliente.getUserProperties().put("suporteAtendimento", proximoSuporte);
                    suportesDisponiveis.remove(proximoSuporte);

                    if (sSuporte != null) {
                        Connection con = null;
                        try {
                            GerarConexao gCon = new GerarConexao();
                            con = gCon.novaConexao("jdbc:postgresql://192.168.0.252:5433/MATEUS_INOVACAO_HOMEM", "postgres", "postgres");
                            GwChatBO bo = new GwChatBO(con);

                            Ocorrencia ocorrencia = new Ocorrencia();
                            ocorrencia.setIdCliente(c.getIdCliente());
                            ocorrencia.setStatus_ocorrencia("P");
                            ocorrencia.setTipoCorrencia(1);
                            ocorrencia.setLoginSuporte(proximoSuporte);

                            String protocolo = bo.cadastrarOcorrencia(ocorrencia);

                            sSuporte.getUserProperties().put("clienteAtendimento", proximoCliente);
                            //remove da sala de fila para por na de atendimento
                            filaEsperaAtendimento.remove(proximoCliente);
                            //poe na sala de atendimento
                            ChatServerCliente.clientesEmAtendimento.put(proximoCliente, sCliente);
                            ChatServerSuporte.suportesEmAtendimento.put(proximoSuporte, sSuporte);
                            sSuporte.getBasicRemote().sendText(MSG_SUPORTE_INICIO_ATENDIMENTO.replaceAll("<nmCliente>", proximoCliente).replaceAll("<nmSuporte>", proximoSuporte).replaceAll("<N� Protocolo>",protocolo));
                            sCliente.getBasicRemote().sendText(MSG_SAUDACAO.replaceAll("<nmCliente>", proximoCliente).replaceAll("<nmSuporte>", proximoSuporte).replaceAll("<N� Protocolo>", protocolo));
                            ChatServerCliente.atualizarFilaEspera(filaEsperaAtendimento);
                        } finally {
                            if (con != null) {
                                con.close();
                            }
                        }
                    }
                }
            }

        } catch (Exception ex) {
            System.out.println("EX : " + ex);
        }
    }

    public Cliente montarCliente(Session session) {
        Map<String, List<String>> parametros = session.getRequestParameterMap();

        String login = parametros.get(USER).get(0);
        int id = Integer.parseInt(parametros.get("id").get(0));
        String cnpj = parametros.get("cnpj").get(0);
        String email = parametros.get("email").get(0);
        String modulo = parametros.get("modulo").get(0);

        System.out.println("CHEGOU o : " + id);
        Cliente c = new Cliente(login, email, cnpj, modulo, id);

        return c;
    }

    public static void atualizarFilaEspera(Map<String, Session> filaEsperaAtendimento) {
        try {
            if (!filaEsperaAtendimento.isEmpty()) {
                List<String> indexesClientes = new ArrayList<String>(filaEsperaAtendimento.keySet());
                for (String proximoCliente : indexesClientes) {
                    Session session = filaEsperaAtendimento.get(proximoCliente);
                    Cliente c = (Cliente) session.getUserProperties().get(USER);
                    List<String> indexes = new ArrayList<String>(filaEsperaAtendimento.keySet());
                    session.getBasicRemote().sendText(MSG_GANHOU_POSICAO + (indexes.indexOf(c.getNome()) + 1));
                }
            }

        } catch (IOException ex) {
            System.out.println("ERRO ATUALIZAR ESPERA : " + ex.getMessage());
        }
    }
}
