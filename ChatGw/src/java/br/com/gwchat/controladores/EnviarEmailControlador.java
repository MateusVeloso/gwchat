/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gwchat.controladores;

import br.com.gwchat.apoio.GerarConexao;
import br.com.gwchat.bo.GwChatBO;
import br.com.gwchat.dao.GwChatDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.mail.*;

/**
 *
 * @author marcus
 */
public class EnviarEmailControlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=ISO-8859-1");
        String acao = request.getParameter("acao");
        PrintWriter out = response.getWriter();

        try {
            if (acao.equalsIgnoreCase("enviarEmail")) {
                this.enviarEmail(request, response);
            }
        } catch (Exception ex) {
            System.out.println("EX: " + ex.getMessage());
            ex.printStackTrace();
        }

    }

    public void enviarEmail(HttpServletRequest request, HttpServletResponse response) throws Exception {
        br.com.gwchat.geral.Email configEmail = null;
        Connection con = null;
        try {
            con = new GerarConexao().novaConexao(request.getServletContext().getInitParameter("base"), request.getServletContext().getInitParameter("usuarioBanco"), request.getServletContext().getInitParameter("senhaBanco"));
            GwChatDAO dao = new GwChatDAO(con);
            configEmail = dao.getConfigEmailRemetente();
            
        } finally {
            if (con != null && !con.isClosed()) {
                con.close();
            }
        }
        
        String destinatario = request.getParameter("email");
        String protocolo = request.getParameter("protocolo");

        
        HtmlEmail email = new HtmlEmail();
        email.setHostName(configEmail.getMail_servidor());
        email.setFrom(destinatario, "Atendimento gwChat protocolo " + protocolo);
        email.addTo(destinatario, "Atendimento gwChat protocolo " + protocolo);
        email.setAuthentication(configEmail.getMail_usuario(), configEmail.getMail_senha());
        email.setSubject("Atendimento gwChat protocolo" + protocolo);
        if (configEmail.getMail_smtp_porta() != 0) {
            email.setSmtpPort(configEmail.getMail_smtp_porta());
        }
        email.setSSLOnConnect(true);

        StringBuilder html = new StringBuilder();

        html.append("<html>");
        html.append("<head>");
        html.append("<style>");
        html.append(".geral{width:100%;float:left;background: #ccc;}");
        html.append(".div-topo{width:100%;background:#0d2d4a;text-align:center;color:#fff;font-size:19px;font-weight: bold;padding-top: 10px;padding-bottom: 10px;float: left;}");
        html.append(".div-corpo-msg-cliente{width:60%;background:#ccc;text-align:left;color:#333;font-size:14px;font-weight:bold;float: left;padding-top:5px;}");
        html.append(".div-corpo-msg-cliente label{margin-left: 10px;}");
        html.append(".div-corpo-msg-suporte{width:60%;background:#ccc;text-align:left;color:#333;font-size:14px;font-weight:bold;float: right;padding-top:5px;}");
        html.append(".div-corpo-msg-suporte label{margin-left: 10px;}");
        html.append("</style>");
        html.append("</head>");

        html.append("<body>");
        html.append("<div class='geral'>");
        html.append("<div class='div-topo'>GWChat - Atendimento Online</div>");
        html.append("<div><center>Protocolo : ").append(protocolo).append("</center></div>");
//        byte array[] = request.getParameter("conversa").replaceAll("gw-chat-B-cinza", "div-corpo-msg-suporte").replaceAll("gw-chat-B-azul", "div-corpo-msg-cliente").replaceAll("<img src=\"img/visualizado.png\">", "").replaceAll("<ul>", "").replaceAll("</ul>", "").replaceAll("<li>", "</li>").getBytes("UTF-8");
//        String novaString = new String(array, "ISO-8859-1");
//        html.append(novaString);
//        html.append("<div class='div-corpo-msg-cliente'><label>Erika : Oi Mateus eu te amo</label></div>");
//        html.append("<div class='div-corpo-msg-suporte'><label>Suporte : Sai feia.</label></div>");
        html.append("<div>");
        html.append("</body>");

        html.append("</html>");

        email.setHtmlMsg(html.toString());

        // set the alternative message
        email.setTextMsg("Your email client does not support HTML messages");

        // send the email
        email.send();
    }
    
    public void getDadosEmailRemetente(HttpServletRequest request){
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
