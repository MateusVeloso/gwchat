package br.com.gwchat.apoio;

import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mateus
 */
public class Auxiliar {

    public static Properties lerProperties(String caminhoArquivo) {
        Properties prop = new Properties();

        try {
            FileInputStream arquivoIn = new FileInputStream(caminhoArquivo);
            prop.load(arquivoIn);
            arquivoIn.close();

        } catch (Exception ex) {
            System.out.println("ERRO CONEXAO : " + ex.getMessage());
            Logger.getLogger(Auxiliar.class.getName()).log(Level.SEVERE, null, ex);
        }

        return prop;
    }

//    public static void main(String[] args) {
//    }
    public static String getHoraAtual() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date hora = Calendar.getInstance().getTime(); // Ou qualquer outra forma que tem
        String dataFormatada = sdf.format(hora);

        return dataFormatada;
    }

    public static String getSaudacao() {
        String saudacao = "";
        int hr = Integer.parseInt(getHoraAtual().split(":")[0]);
        
        if (hr < 12){
            saudacao = "Bom dia";
        }else if (hr >= 12 && hr < 18) {
            saudacao = "Boa tarde";
        }else if (hr >= 18 && hr < 24) {
            saudacao = "Boa noite";
        }

        System.out.println("CHEGOU : " + saudacao);
        return saudacao;
    }

}
