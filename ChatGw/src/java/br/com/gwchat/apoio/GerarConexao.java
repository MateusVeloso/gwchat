package br.com.gwchat.apoio;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import javax.servlet.ServletContext;

/**
 *
 * @author Mateus
 */
public class GerarConexao {

    public Connection novaConexao(String base, String usuario, String senha) {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection(base, usuario, senha);

        } catch (Exception ex) {
            System.out.println("ERRO CONEXAO : " + ex.getMessage());
        }

        return con;
    }

}
