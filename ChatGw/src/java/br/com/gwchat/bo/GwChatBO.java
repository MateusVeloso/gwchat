package br.com.gwchat.bo;

import br.com.gwchat.apoio.GerarConexao;
import br.com.gwchat.dao.GwChatDAO;
import br.com.gwchat.geral.Ocorrencia;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;

/**
 *
 * @author Mateus
 */
public class GwChatBO {
    Connection con;

    public GwChatBO(Connection con) {
        this.con = con;
    }

    public int existeCnpjSerial(String cnpjSerial) throws SQLException {
        return new GwChatDAO(con).existeCnpjSerial(cnpjSerial);
    }

    public String cadastrarOcorrencia(Ocorrencia ocorrencia) throws SQLException {
        return new GwChatDAO(con).cadastrarOcorrencia(ocorrencia);
    }
    
    public Collection<String> carregarMotivosOcorrencia(String sistema) throws Exception{
        return new GwChatDAO(con).carregarMotivosOcorrencia(sistema);
    }
    
    public void gravarMotivoOcorrencia(String motivo, String observacao, String protocolo) throws Exception{
        GwChatDAO dao = new GwChatDAO(con);
        dao.gravarMotivoOcorrencia(motivo,observacao,protocolo);
    }
    
    public void gravarConversa(String conversa, String protocolo) throws Exception{
        GwChatDAO dao = new GwChatDAO(con);
        conversa = conversa.replaceAll("\r", ""); 
        conversa = conversa.replaceAll("\t", "");
        conversa = conversa.replaceAll("\n", "");
        
        dao.gravarConversa(conversa,protocolo);
    }

}
