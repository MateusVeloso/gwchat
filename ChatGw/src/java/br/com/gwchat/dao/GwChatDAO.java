package br.com.gwchat.dao;

import br.com.gwchat.geral.Email;
import br.com.gwchat.geral.Ocorrencia;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Mateus
 */
public class GwChatDAO {

    Connection con;

    public GwChatDAO(Connection con) {
        this.con = con;
    }

    public int existeCnpjSerial(String cnpjSerial) throws SQLException {
        StringBuilder sql = new StringBuilder();
        
        int idCliente = 0;
        
        if (cnpjSerial.length() > 8) {
            sql.append("select idcliente from cliente where cnpj = ?");
        }else{
            sql.append("select c.idcliente from cliente c left join contrato_sistema cs ON (c.cnpj = cs.cnpj) where cs.numero_serial = ?");
        }
        
        int index = 0;
        PreparedStatement ppStm = con.prepareStatement(sql.toString());
        ppStm.setString(++index, cnpjSerial);

        ResultSet rs = ppStm.executeQuery();

        while (rs.next()) {
            idCliente = rs.getInt("idcliente");
        }
        return idCliente;
    }
    
    
    public String cadastrarOcorrencia(Ocorrencia ocorrencia) throws SQLException{
        String protocolo = "";
        
//        String sql = "INSERT INTO crm_ocorrencia (usuario_id,usuario_preferencial_id,tipo_ocorrencia_id,cliente_id,data,hora,descricao,status_ocorrencia)"
//        + " values (SELECT idusuario FROM usuario WHERE login = ?,SELECT idusuario FROM usuario WHERE login = ?,?,?,CURRENT_DATE,CURRENT_TIME,'GWCHAT"+ocorrencia.getIdCliente()+"','P')";
        
        String sql = "INSERT INTO crm_ocorrencia (usuario_id,usuario_preferencial_id,tipo_ocorrencia_id,cliente_id,data,hora,descricao,status_ocorrencia) "
        + " values ((SELECT idusuario FROM usuario WHERE login = ?),(SELECT idusuario FROM usuario WHERE login = ?),?,?,CURRENT_DATE,CURRENT_TIME,'Testando','P') returning protocolo::text";
        
        PreparedStatement ppStm = con.prepareStatement(sql);  
        int index = 0;
        ppStm.setString(++index, ocorrencia.getLoginSuporte());
        ppStm.setString(++index, ocorrencia.getLoginSuporte());
        ppStm.setInt(++index, ocorrencia.getTipoCorrencia());
        ppStm.setInt(++index, ocorrencia.getIdCliente());
        
        ResultSet rs = ppStm.executeQuery();
        
        if (rs.next()) {
            protocolo = rs.getString("protocolo");
        }
        
        return protocolo;
    }
    
    public Collection<String> carregarMotivosOcorrencia(String sistema)throws Exception{
        Collection<String> listaMotivos = new ArrayList<>();
        
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT motivo FROM crm_motivos WHERE sistema = ?;");
        
        int index = 0;
        
        PreparedStatement ppStm = con.prepareStatement(sql.toString());
        ppStm.setString(++index, sistema);
        
        ResultSet rs = ppStm.executeQuery();
        
        while(rs.next()){
            listaMotivos.add(rs.getString("motivo"));
        }
        
        return listaMotivos;
    }
    
    public void gravarMotivoOcorrencia(String motivo,String observacao,String protocolo) throws Exception{
        
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO crm_historico_ocorrencia (idOcorrencia,motivo,observacao,status) VALUES ");
        sql.append("((select id from crm_ocorrencia where protocolo = ").append(protocolo).append("),?,?,?);");

        int index = 0;
        PreparedStatement ppStm = con.prepareStatement(sql.toString());
        ppStm.setString(++index, motivo);
        ppStm.setString(++index, observacao);
        ppStm.setString(++index, "F");
        
        System.out.println("SQL : " + ppStm.toString());
        ppStm.execute();
    }
    
    public void gravarConversa(String conversa,String protocolo) throws Exception{
        
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO gw_chat.conversas_chat (id_ocorrencia,conversa,data,hora) VALUES ");
        sql.append("((select id from crm_ocorrencia where protocolo = ").append(protocolo).append(")");
        sql.append(",?,CURRENT_DATE,CURRENT_TIME);");

        int index = 0;
        PreparedStatement ppStm = con.prepareStatement(sql.toString());
        ppStm.setString(++index, conversa);
        
        System.out.println("SQL : " + ppStm.toString());
        ppStm.execute();
    }
    
    public Email getConfigEmailRemetente() throws Exception{
        Email email = null;
        
        StringBuilder sql = new StringBuilder();
        sql.append("select * from config_emails WHERE id = (SELECT mail_envio_conversa FROM gw_chat.config_chat limit 1)");
        
        PreparedStatement ppStm = con.prepareStatement(sql.toString());
        ResultSet rs = ppStm.executeQuery();
        
        if (rs.next()) {
            email = new Email();
            email.setMail_servidor(rs.getString("mail_servidor"));
            email.setMail_remetente(rs.getString("mail_remetente"));
            email.setMail_nome_remetente(rs.getString("mail_nome_remetente"));
            email.setMail_smtp_porta(rs.getInt("mail_smtp_porta"));
            email.setMail_usuario(rs.getString("mail_usuario"));
            email.setMail_senha(rs.getString("mail_senha"));
            email.setIs_mail_autenticado(rs.getString("is_mail_autenticado"));
            email.setIs_starttls(rs.getString("is_starttls"));
            email.setIs_ssl(rs.getString("is_ssl"));
        }
        return email;
    }

}
