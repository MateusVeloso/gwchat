package br.com.gwchat.geral;

/**
 *
 * @author Mateus
 */
public class Cliente {

    private int idCliente;
    private String nome;
    private String email;
    private String cnpj;
    private String modulo;

    public Cliente(String nome, String email, String cnpj, String modulo, int idCliente) {
        this.nome = nome;
        this.email = email;
        this.cnpj = cnpj;
        this.modulo = modulo;
        this.idCliente = idCliente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

}