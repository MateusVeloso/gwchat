package br.com.gwchat.geral;

import java.util.Collection;

/**
 *
 * @author Mateus
 */
public class Suporte {
    
    private int id;
    private String nome;
    private String login;
    private String senha;
    private String nomeClienteAtendimento;
    private String email;
    private String status;
    private Collection<String> modulos;
    
    public Suporte(int id, String nome, String login, String senha,String nomeClienteAtendimento,String status, Collection<String> modulos, String email) {
        this.id = id;
        this.nome = nome;
        this.login = login;
        this.senha = senha;
        this.nomeClienteAtendimento = nomeClienteAtendimento;
        this.status = status;
        this.modulos = modulos;
        this.email = email;
    }

    public Suporte(){
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNomeClienteAtendimento() {
        return nomeClienteAtendimento;
    }

    public void setNomeClienteAtendimento(String nomeClienteAtendimento) {
        this.nomeClienteAtendimento = nomeClienteAtendimento;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Collection<String> getModulos() {
        return modulos;
    }

    public void setModulos(Collection<String> modulos) {
        this.modulos = modulos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
}