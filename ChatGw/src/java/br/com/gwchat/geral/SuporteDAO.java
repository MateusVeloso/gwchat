package br.com.gwchat.geral;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Mateus
 */
public class SuporteDAO {

    Connection con;

    public SuporteDAO(Connection con) {
        this.con = con;
    }

    public Suporte logarSuporte(String login, String senha) throws SQLException {
        Suporte suporte = null;
        StringBuilder sql = new StringBuilder();

        sql.append("select * from usuario where login = ? and senha = ? and  is_usuario_crm = true;");
        int index = 0;
        PreparedStatement ppStm = con.prepareStatement(sql.toString());
        ppStm.setString(++index, login);
        ppStm.setString(++index, senha);

        ResultSet rs = ppStm.executeQuery();
        Object[] objModulos = null;
        Array rsArray;
        Collection modulos = new ArrayList<String>();
        while (rs.next()) {
            if (rs.getString("senha").equals(senha)) {
                suporte = new Suporte();
                suporte.setNome(rs.getString("nome"));
                suporte.setEmail(rs.getString("email"));
                suporte.setLogin(rs.getString("login"));
                suporte.setSenha(rs.getString("senha"));
                suporte.setStatus("offline");
                //----
//                rsArray = rs.getArray("modulos");
//                objModulos = (Object[]) rsArray.getArray();
//                
//                for (int i = 0; i < objModulos.length; i++) {
//                    modulos.add(objModulos[i]);
//                }

                modulos.add(new String("trans"));
                
                suporte.setModulos(modulos);
            }
        }

        return suporte;
    }
}