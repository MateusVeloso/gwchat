package br.com.gwchat.geral;

/**
 *
 * @author Mateus
 */
public class Email {

    String mail_servidor,
            mail_remetente,
            mail_nome_remetente,
            mail_usuario,
            mail_senha,
            is_mail_autenticado,
            is_starttls,
            is_ssl,
            is_fatura;
    
    int mail_smtp_porta;

    public Email(String mail_servidor, String mail_remetente, String mail_nome_remetente, String mail_usuario, String mail_senha, String is_mail_autenticado, String is_starttls, String is_ssl, String is_fatura, int mail_smtp_porta) {
        this.mail_servidor = mail_servidor;
        this.mail_remetente = mail_remetente;
        this.mail_nome_remetente = mail_nome_remetente;
        this.mail_usuario = mail_usuario;
        this.mail_senha = mail_senha;
        this.is_mail_autenticado = is_mail_autenticado;
        this.is_starttls = is_starttls;
        this.is_ssl = is_ssl;
        this.is_fatura = is_fatura;
        this.mail_smtp_porta = mail_smtp_porta;
    }

    public Email() {
    }
    
    public String getMail_servidor() {
        return mail_servidor;
    }

    public void setMail_servidor(String mail_servidor) {
        this.mail_servidor = mail_servidor;
    }

    public String getMail_remetente() {
        return mail_remetente;
    }

    public void setMail_remetente(String mail_remetente) {
        this.mail_remetente = mail_remetente;
    }

    public String getMail_nome_remetente() {
        return mail_nome_remetente;
    }

    public void setMail_nome_remetente(String mail_nome_remetente) {
        this.mail_nome_remetente = mail_nome_remetente;
    }

    public String getMail_usuario() {
        return mail_usuario;
    }

    public void setMail_usuario(String mail_usuario) {
        this.mail_usuario = mail_usuario;
    }

    public String getMail_senha() {
        return mail_senha;
    }

    public void setMail_senha(String mail_senha) {
        this.mail_senha = mail_senha;
    }

    public String getIs_mail_autenticado() {
        return is_mail_autenticado;
    }

    public void setIs_mail_autenticado(String is_mail_autenticado) {
        this.is_mail_autenticado = is_mail_autenticado;
    }

    public String getIs_starttls() {
        return is_starttls;
    }

    public void setIs_starttls(String is_starttls) {
        this.is_starttls = is_starttls;
    }

    public String getIs_ssl() {
        return is_ssl;
    }

    public void setIs_ssl(String is_ssl) {
        this.is_ssl = is_ssl;
    }

    public String getIs_fatura() {
        return is_fatura;
    }

    public void setIs_fatura(String is_fatura) {
        this.is_fatura = is_fatura;
    }

    public int getMail_smtp_porta() {
        return mail_smtp_porta;
    }

    public void setMail_smtp_porta(int mail_smtp_porta) {
        this.mail_smtp_porta = mail_smtp_porta;
    }
    
    

}
