package br.com.gwchat.geral;

import java.util.Date;

/**
 *
 * @author marcus
 */
public class Ocorrencia {
    
    private int idSuporte;
    private int tipoCorrencia;
    private int idCliente;
    private Date data;
    private Date hora;
    private String descricao;
    private String status_ocorrencia;
    private String loginSuporte;

    public Ocorrencia(int idSuporte, int tipoCorrencia, int idCliente, Date data, Date hora, String descricao, String status_ocorrencia, String loginSuporte) {
        this.idSuporte = idSuporte;
        this.tipoCorrencia = tipoCorrencia;
        this.idCliente = idCliente;
        this.data = data;
        this.hora = hora;
        this.descricao = descricao;
        this.status_ocorrencia = status_ocorrencia;
        this.loginSuporte = loginSuporte;
    }
    
    public Ocorrencia(){
        
    }

    public int getIdSuporte() {
        return idSuporte;
    }

    public void setIdSuporte(int idSuporte) {
        this.idSuporte = idSuporte;
    }

    public int getTipoCorrencia() {
        return tipoCorrencia;
    }

    public void setTipoCorrencia(int tipoCorrencia) {
        this.tipoCorrencia = tipoCorrencia;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getStatus_ocorrencia() {
        return status_ocorrencia;
    }

    public void setStatus_ocorrencia(String status_ocorrencia) {
        this.status_ocorrencia = status_ocorrencia;
    }

    public String getLoginSuporte() {
        return loginSuporte;
    }

    public void setLoginSuporte(String loginSuporte) {
        this.loginSuporte = loginSuporte;
    }
    
}
