package br.com.gwchat.geral;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Mateus
 */
public class SuporteBO {
    Connection con = null;
    
    public SuporteBO(Connection con){
        this.con = con;
    }
    
    public Suporte logarSuporte(String login, String senha) throws SQLException{
        return new SuporteDAO(con).logarSuporte(login,senha);
    }
}
