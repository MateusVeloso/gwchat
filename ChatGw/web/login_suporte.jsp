<%-- 
    Document   : login
    Created on : 26/10/2016, 22:25:43
    Author     : Mateus
--%>
<%if (request.getParameter("suporte") != null) {
        response.sendRedirect("index");
    }%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Gw Chat - Login</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="css/material.min.css" rel="stylesheet" type="text/css"/>
        <script src="js/material.min.js" type="text/javascript"></script>
        <script src="js/jquery.js" type="text/javascript"></script>
    </head>
    <body>
        <style>
            .container-login{
                position: absolute;
                width: 500px;
                height: 300px;
                top: 50%;
                left: 50%;
                border-radius: 5px;
                margin-top: -150px;
                margin-left: -250px;
                background: #3b5998;
                -webkit-box-shadow: 0px 0px 30px 1px rgba(0,0,0,0.75);
                -moz-box-shadow: 0px 0px 30px 1px rgba(0,0,0,0.75);
                box-shadow: 0px 0px 30px 1px rgba(0,0,0,0.75);
            }

            .topo-autenticacao{
                color: #fff;
                font-size: 20px;
                font-weight: bold;
                font-family: "Helvetica","Arial",sans-serif !important;
            }
            .footer-autenticacao{
                color: #fff;
                font-size: 12px;
                font-weight: bold;
                font-family: "Helvetica","Arial",sans-serif !important;
            }
        </style>
        <script>
            if (<%=request.getAttribute("erro") != null%>) {
                alert('Suporte n�o encontrado.');
            }
        </script>
        <div class="container-login">
            <div class="mdl-grid">
                <div class="mdl-cell mdl-cell--12-col">
                    <center><label class="topo-autenticacao">AUTENTICA��O</label></center>
                </div>
                <div class="mdl-cell mdl-cell--12-col" style="padding: 10px;background: #ebebeb;height: 200px;border-radius: 5px;">
                    <form action="GwChatControlador?acao=logar_suporte" autocomplete="off" method="post" id="form" name="form">
                        <div class="mdl-textfield mdl-js-textfield" style="width: 100%;">
                            <input class="mdl-textfield__input" autocomplete="new-password" type="text" id="login" name="login">
                            <label class="mdl-textfield__label" for="login">Login...</label>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield" style="width: 100%;">
                            <input class="mdl-textfield__input" autocomplete="new-password" type="password" id="senha" name="senha">
                            <label class="mdl-textfield__label" for="senha">Senha...</label>
                        </div>
                        <center>
                            <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" onclick="form.submit()">
                                Entrar
                            </button>
                        </center>
                    </form>
                </div>
                <div class="mdl-cell mdl-cell--12-col">
                    <label class="footer-autenticacao">Se voc� n�o � um usu�rio tipo suporte (<a href="">Click aqui!</a>)</label>
                </div>
            </div>

        </div>
    </body>
</html>
