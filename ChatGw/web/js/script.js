var socket = null;

function conectarCliente(id, nome, cnpj, email, modulo) {
    abriuConexao();
    socket = new WebSocket('ws://192.168.15.10:8084/gwChat/chatServer?id=' + id + '&nome_usuario=' + nome + '&cnpj=' + cnpj + '&email=' + email + '&modulo=' + modulo + '&is_cliente=true');
    socket.onmessage = socketOnMessage;
    socket.onclose = socketOnClose;
    socket.onerror = socketOnError;
}

function socketOnError(error) {
    console.log(error);
//    window.location.reload()
}

function socketOnMessage(e) {
    var txt = e.data;
    javisualizou = false;

    var strError = "!@!Gw Error#@#";
    if (txt.indexOf(strError) !== -1) {
        setInterval(function () {
            parent.window.location.reload();
        }, 3000);
    }

    if (txt == 'VYZUALYS0U') {
        jQuery('.gw-chat-B-azul img').show(400);
        return false;
    }

    if (txt.split('!@!')[1] != undefined) {
        var horaMsg = txt.split('!@!')[0];
        var nm = txt.split('!@!')[1].split('#@#')[0];
        var msg = txt.split('!@!')[1].split('#@#')[1];

        if (jQuery('.gw-chat-A-dados-suporte b').html() == 'Fila de Espera' && nm != nome) {
            jQuery('.gw-chat-A-dados-suporte b').html(nm);
        }

        if (nome.toUpperCase().trim() == nm.toUpperCase().trim()) {
            jQuery('.gw-chat-B ul').append('<li><div class="gw-chat-B-azul" style="word-break: break-all;"><span><b>' + horaMsg + ' ' + nm + ':</b></span><br>' + msg.replace('?','<br>') + '<img src="img/visualizado.png"></div></li>');
        } else {
            jQuery('.gw-chat-B ul').append('<li><div class="gw-chat-B-cinza" style="word-break: break-all;"><span><b>' + horaMsg + ' ' + nm + ':</b></span><br><label>' + msg + '</label></div></li>');
        }


    } else {
        jQuery('.gw-chat-B ul').append('<li><div class="gw-chat-B-cinza" style="word-break: break-all;"><span><b>' + horaMsg + ' ' + nm + '</b></span><br><label>' + msg + '</label></div></li>');
    }

    acompanharScroll();
}


function socketOnClose(e) {
    jQuery('#divMenssagens').hide('slow');
    jQuery('.cobre-container').show('slow');
    jQuery('#btDesconectar').attr('disabled', 'true');
    jQuery('#menssagem').attr('disabled', 'true');
}

function desconectar() {
    var r = confirm("Tem certeza que deseja encerrar esse atendimento?");
    if (r == true) {
        socket.close();
        socket = null;

        parent.restaurar();
        jQuery('.gw-img-perfil-suporte').css('display', 'block');
        jQuery('.gw-chat-A-dados-suporte').css('display', 'block');
        jQuery('.gw-chat-A-buttons').css('display', 'block');
        jQuery('.gw-chat-A-topo').css('cursor', 'default');

        jQuery('.gw-chat').css('width', '290px');
        jQuery('.gw-chat').css('height', '410px');
        jQuery('.gw-chat-C').css('height', '82px');
        jQuery('.gw-chat-C-txt-msg').css('height', '30px');
        jQuery('.gw-chat-C-btn-enviar-msg').css('margin-left', '10px');
        jQuery('.gw-chat-C-lb-chk-enter').css('margin-right', '26px');


        jQuery('.gw-chat-C').hide(200);
        jQuery('.gw-chat-B').hide(200);
        jQuery('.gw-chat-A-buttons').hide(200);
        jQuery('.gw-chat-B-question').show(400);
    }
}

function enviarMenssagem(evento, elemento) {
    if (evento.keyCode == '13' && jQuery('#gw-chat-C-chk-enter').prop('checked') == true) {
        if (socket != null && jQuery('.gw-chat-C-txt-msg').val().trim() != "") {
            socket.send(jQuery(elemento).val());
            jQuery(elemento).val('');
            acompanharScroll();
        }
    }
}

function enviarMenssagemClick() {
    if (jQuery('.gw-chat-C-txt-msg').val().trim() != "") {
        socket.send(jQuery('.gw-chat-C-txt-msg').val());
        jQuery('.gw-chat-C-txt-msg').val('');
    }
}

function novoChamado() {
    window.location = "loginCliente.jsp";
}

function enviarConversaEmail() {

}

function acompanharScroll() {
    jQuery('.gw-chat-B').animate({
        scrollTop: jQuery('.gw-chat-B ul').height()
    }, 500);
}

var javisualizou = false;
jQuery(document).ready(function () {

    jQuery('.gw-chat').hover(
            function () {
                if (jQuery('.gw-chat-B ul li').last().find('div').attr('class') == "gw-chat-B-cinza" && !javisualizou) {
                    socket.send("VYZUALYS0U");
                    javisualizou = true;
                }
            }
    );


    jQuery('.gw-chat-A-topo').click(function () {
        if (!parent.isAberto()) {
            parent.abrir();
            jQuery('.gw-img-perfil-suporte').css('display', 'block');
            jQuery('.gw-chat-A-dados-suporte').css('display', 'block');
            jQuery('.gw-chat-A-buttons').css('display', 'block');
            jQuery('.gw-chat-A-topo').css('cursor', 'default');
        }
    });

    jQuery('.gw-chat-A-minimizar').click(function () {
        parent.minimizar();
        jQuery('.gw-img-perfil-suporte').css('display', 'none');
        jQuery('.gw-chat-A-dados-suporte').css('display', 'none');
        jQuery('.gw-chat-A-buttons').css('display', 'none');
        jQuery('.gw-chat-A-topo').css('cursor', 'pointer');

        jQuery('.gw-chat').css('width', '290px');
        jQuery('.gw-chat').css('height', '410px');
        jQuery('.gw-chat-C').css('height', '82px');
        jQuery('.gw-chat-C-txt-msg').css('height', '30px');
        jQuery('.gw-chat-C-btn-enviar-msg').css('margin-left', '10px');
        jQuery('.gw-chat-C-lb-chk-enter').css('margin-right', '26px');

    });

    jQuery('.gw-chat-A-maxminizar').click(function () {
        maxminizarChat();
    });

    jQuery('.gw-chat-A-fechar').click(function () {
        desconectar();
    });
});

function abriuConexao() {
    parent.minimizar();
    jQuery('.gw-img-perfil-suporte').css('display', 'none');
    jQuery('.gw-chat-A-dados-suporte').css('display', 'none');
    jQuery('.gw-chat-A-buttons').css('display', 'none');
    jQuery('.gw-chat-A-topo').css('cursor', 'pointer');

    jQuery('.gw-chat').css('width', '290px');
    jQuery('.gw-chat').css('height', '410px');
    jQuery('.gw-chat-C').css('height', '82px');
    jQuery('.gw-chat-C-txt-msg').css('height', '30px');
    jQuery('.gw-chat-C-btn-enviar-msg').css('margin-left', '10px');
    jQuery('.gw-chat-C-lb-chk-enter').css('margin-right', '26px');

    setTimeout(function () {
        parent.abrir();
        jQuery('.gw-img-perfil-suporte').css('display', 'block');
        jQuery('.gw-chat-A-dados-suporte').css('display', 'block');
        jQuery('.gw-chat-A-buttons').css('display', 'block');
        jQuery('.gw-chat-A-topo').css('cursor', 'default');
    }, 500);
}

function chatMaxminizado() {

}

function maxminizarChat() {
    if (jQuery('.gw-chat').css('width') != '500px') {
        parent.maxminizar();
        jQuery('.gw-chat').css('width', '500px');
        jQuery('.gw-chat').css('height', '450px');
        jQuery('.gw-chat-C').css('height', '115px');
        jQuery('.gw-chat-C-txt-msg').css('height', '60px');
        jQuery('.gw-chat-C-btn-enviar-msg').css('margin-left', '20px');
        jQuery('.gw-chat-C-lb-chk-enter').css('margin-right', '118px');
    } else {
        parent.restaurar();
        jQuery('.gw-chat').css('width', '290px');
        jQuery('.gw-chat').css('height', '410px');
        jQuery('.gw-chat-C').css('height', '82px');
        jQuery('.gw-chat-C-txt-msg').css('height', '30px');
        jQuery('.gw-chat-C-btn-enviar-msg').css('margin-left', '10px');
        jQuery('.gw-chat-C-lb-chk-enter').css('margin-right', '26px');
    }
}

jQuery('.gw-chat-C-btn-enviar-msg').css('height', '30px !important');

function finalizar() {
    if (jQuery('#checkbox-1').prop('checked')) {
//        jQuery('.gw-chat-B ul li').html();
        jQuery.ajax({
            url: 'EnviarEmailControlador?acao=enviarEmail&email=' + email + '&conversa=' + jQuery('.gw-chat-B ul').html()
        });
    }

    window.location = 'login-cliente';
}

jQuery(window).on('beforeunload ', function () {
    if (socket != null) {
        return 'Tem certeza que deseja encerrar esse atendimento? Se sim, o atendimento ser� encerrado automaticamente.';
    }
});