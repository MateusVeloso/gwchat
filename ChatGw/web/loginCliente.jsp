<%-- 
    Document   : cliente
    Created on : 28/10/2016, 00:36:18
    Author     : Mateus
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <script src="js/jquery.js" type="text/javascript"></script>
        <script src="js/material.min.js" type="text/javascript"></script>
        <link href="css/material.min.css" rel="stylesheet" type="text/css"/>

        <title>Gw Chat - Login Cliente</title>
        <!--<embed src="http://192.168.124:8080/gwChat/cliente" style="position: absolute;bottom: 0;right: 10%;z-index: 9999999999;width: 350px;height: 400px;">-->
        <style>
            html,body{
                margin: 0;
                padding: 0;
                overflow: hidden !important;
            }
            .container-chat{
                height: 320px;
                width: 260px;
                background: #fff;
                overflow: hidden;
                border: 0.5px solid rgba(0,0,0,0.2);
            }
            .topo-container{
                border-radius: 7px 7px 0 0;
                width: 100%;
                float: left;
                padding-top: 4px;
                padding-bottom: 4px;
                padding-left: 10px;
                background: rgba(19,59,92,1);
                cursor: pointer;
                /*margin-bottom: 5px;*/
            }
            .topo-container:hover{
                background: rgba(19,59,92,0.9);
            }
            .lb-topo{
                width: 80%;
                float: left;
                color: #fff;
                font-family: inherit;
                font-weight: bold;
                font-size: 13px;
                /*font-family: "Helvetica","Arial",sans-serif !important;*/
            }

            .corpo-container{
                width: 100%;
                float: left;
                height: 360px;
                overflow-y: scroll;
                overflow-x: hidden;
                /*background: #fff;*/
            }

            .cobre-container{
                width: 100%;
                height: 360px;
                background: url(img/pattern_bg.png);
            }

            .inputCnpjSerial{
                width: 80%;
                border-radius: 5px;
                font-size: 12px;
                padding: 5px;
            }

            .inputCnpjSerial:focus{
                outline: none;
            }

            .div-btn-validar{
                width: 92%;
                border-radius: 5px;
                height: 28px;
                color:#fff;
                background: rgba(19,59,92,1);
                cursor: pointer;
            }

            .div-btn-validar:hover{
                -webkit-box-shadow: 0px 0px 1px 1px rgba(0,0,0,0.3);
                -moz-box-shadow: 0px 0px 1px 1px rgba(0,0,0,0.3);
                box-shadow: 0px 0px 1px 1px rgba(0,0,0,0.3);
                background: rgba(19,59,92,0.9);
            }

            .div-btn-validar center{
                float: left;
                text-align: center;
                width: 100%;
                margin-top: 4px;
                cursor: pointer;
            }



            /*            .cobre-container input{
                            margin-top: 10px;
                            width: 90%;
                            font-size: 12px;
                            padding-left: 2px;
                            box-shadow: none;
                            color: #666;
                        }*/
            .lb-radio > input{
                visibility: hidden; 
                position: absolute;
            }
            .lb-radio > input + img{
                cursor:pointer;
                border:2px solid transparent;
            }
            .lb-radio > input:checked + img{
                border:2px solid rgba(66,103,178,0.5);
            }

            .lb-radio img{
                width: 70px;
            }

            input:-webkit-autofill 
            {    
                -webkit-box-shadow: 0 0 0px 1000px #f9fbfd inset !important;
                -webkit-text-fill-color: rgba(19,59,92,1) !important;
            }

        </style>
        <script>
            jQuery(document).ready(function () {
                //Desbilitar campos para que exija a autenticacao
                jQuery('#nome').prop('disabled', 'true');
                jQuery('#email').prop('disabled', 'true');
                jQuery('#btn-solicitar').prop('disabled', 'true');
                jQuery('input[name=modulo]').parent('.lb-radio').hide();



                jQuery('.topo-container').click(function () {
                    parent.abrir();
                });

            });

            function validarCnpjSerial() {
                jQuery.ajax({
                    url: 'GwChatControlador?acao=existeCnpjSerial&cnpjSerial=' + jQuery('#cnpjSerial').val(),
                    success: function (data, textStatus, jqXHR) {
                        if (data != null && data.trim() != '') {
                            alert('Foi encontrado o cliente pelo cnpj');
                            jQuery('#nome').prop('disabled', '');
                            jQuery('#email').prop('disabled', '');
                            jQuery('#btn-solicitar').prop('disabled', '');


                            var i = 0;
                            while (data.split('!@!')[i] != null && data.split('!@!')[i] != undefined && data.split('!@!')[i].trim() != '') {
                                jQuery('input[name=modulo][value=' + data.split('!@!')[i] + ']').parent('.lb-radio').show();
                                i++;
                            }

                            //Trava o campo cnpj
                            jQuery('#cnpjSerial').prop('disabled', 'true');
                            jQuery('#bt-validar').prop('disabled', 'true');
                        } else {
                            alert('Cnpj ou Serial n�o encontrado!');
                        }
                    }
                });
            }

            function submitForm() {
                if (jQuery('input[name=modulo]:checked').val() != undefined) {

                    var form = document.getElementById("form");
                    jQuery('#cnpjSerial').prop('disabled', '');

                    form.appendChild(document.getElementById("cnpjSerial"));
                    form.appendChild(document.getElementById("nome"));
                    form.appendChild(document.getElementById("email"));

                    var i = 0;
                    while (document.getElementsByName('modulo')[i] != null) {
                        form.appendChild(document.getElementsByName('modulo')[i]);
                        i++;
                    }
                    parent.conectou();
                    form.submit();
                } else {
                    alert('Selecione um modulo para seu atendimento.');
                }
            }

        </script>
    </head>
    <body>
        <div class="container-chat">
            <div class="topo-container">
                <div class="lb-topo">GW Chat - Suporte</div>
            </div>
            <div class="corpo-container">
                <div class="cobre-container">
                    <form method="POST" action="GwChatControlador?acao=iniciar_chat" id="form">
                    </form>
                    <center style="margin-bottom: 10px;float: left;width: 100%;">
                        <div style="width: 60%;float: left;margin-top: 10px;">
                            <input style="" class="inputCnpjSerial" id="cnpjSerial" placeholder="CNPJ ou SERIAL">
                            <div class="mdl-tooltip" data-mdl-for="cnpjSerial">
                                Cnpj ou Serial
                            </div>
                        </div>
                        <div style="width: 37%;float: left;margin-top: 10px;">
                            <div class="div-btn-validar" id="bt-validar" onclick="validarCnpjSerial();" >
                                <center>Validar</center>
                            </div>
                        </div>
                    </center>
                    <center style="margin-bottom: 10px;float: left;width: 100%;">
                        <input style="width: 88%;" class="inputCnpjSerial" name="nome" id="nome" placeholder="Nome">
                        <div class="mdl-tooltip" data-mdl-for="nome">
                            Nome
                        </div>
                    </center>
                    <center style="margin-bottom: 10px;float: left;width: 100%;">
                        <input style="width: 88%;" id="email" name="email" class="inputCnpjSerial" placeholder="E-mail">
                        <div class="mdl-tooltip" data-mdl-for="email">
                            Email
                        </div>
                    </center>
                    <center style="margin-top: 10px;float: left;width: 100%;">
                        <label class="lb-radio">
                            <input type="radio" name="modulo"  value="trans" />
                            <img src="img/trans_footer.png">
                        </label>
                        <label class="lb-radio">
                            <input type="radio" name="modulo" value="logis"/>
                            <img src="img/logis_footer.png">
                        </label>
                        <label class="lb-radio">
                            <input type="radio" name="modulo" value="frota"/>
                            <img src="img/frota_footer.png">
                        </label>
                        <label class="lb-radio">
                            <input type="radio" name="modulo" value="finan"/>
                            <img src="img/finan_footer.png">
                        </label>
                        <label class="lb-radio">
                            <input type="radio" name="modulo" value="mobile"/>
                            <img src="img/mobile_footer.png">
                        </label>
                        <label class="lb-radio">
                            <input type="radio" name="modulo" value="cloud"/>
                            <img src="img/cloud_footer.png">
                        </label>
                    </center>
                    <center style="margin-top: 10px;float: left;width: 100%;">
                        <div class="div-btn-validar"  id="btn-solicitar" style=""  onclick="submitForm();">
                            <center>Solicitar suporte</center>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </body>
</html>
