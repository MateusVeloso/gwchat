<%-- 
    Document   : monitoramento
    Created on : 28/11/2016, 03:12:52
    Author     : Mateus
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Gw Chat - Monitoramento</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <script src="js/jquery.js" type="text/javascript"></script>
        <script src="js/material.min.js" type="text/javascript"></script>
        <link href="css/material.min.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="demo-layout-waterfall mdl-layout mdl-js-layout">
            <header class="mdl-layout__header mdl-layout__header--waterfall">
                <div class="mdl-layout__header-row">
                    <!-- Title -->
                    <span class="mdl-layout-title">Gw Chat</span>
                </div>
            </header>
            <div class="mdl-layout__drawer">
                <span class="mdl-layout-title">Gw Chat</span>
                <nav class="mdl-navigation">
                    <a class="mdl-navigation__link" href="#" onclick="desconectar();">Sair</a>
                </nav>
            </div>
            <main class="mdl-layout__content">
                <div class="page-content" >
                    <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect" >
                        <div class="mdl-tabs__tab-bar">
                            <a href="#fila-espera" class="mdl-tabs__tab is-active">Suporte - Gw Chat</a>
                        </div>
                        <div class="mdl-tabs__panel is-active" id="fila-espera">
                            <div style="width: 100%;background: #e9ebee;">
                                <div class="mdl-grid">
                                    <div class="mdl-cell mdl-cell--6-col">
                                        <div class="container-serie" style="float: left;">
                                            <div style="padding: 15px;width: 100%;background: #fff;border-radius: 5px;float: left;">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </main>
        </div>
    </body>
</html>
