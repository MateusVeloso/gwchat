DROP TABLE IF EXISTS cliente;
CREATE TABLE cliente (
	idcliente serial not null,
	nome varchar,
	cnpj varchar
);

INSERT INTO cliente (nome,cnpj) values ('MJS','123456789');
INSERT INTO cliente (nome,cnpj) values ('FOCUS','111111111');


DROP TABLE IF EXISTS contrato_sistema;
CREATE TABLE contrato_sistema (
	idcliente int,
	cnpj varchar,
	numero_serial varchar	
);

INSERT INTO contrato_sistema (idcliente,cnpj,numero_serial) VALUES (1,'123456789','A1B1');
INSERT INTO contrato_sistema (idcliente,cnpj,numero_serial) VALUES (2,'111111111','A2B2');

DROP TABLE IF EXISTS crm_ocorrencia;
CREATE TABLE crm_ocorrencia(
	id serial not null,
	usuario_id int,
	usuario_preferencial_id int,
	tipo_ocorrencia_id int,
	cliente_id int,
	data date,
	hora date,
	descricao varchar,
	status_ocorrencia varchar,
	protocolo serial not null
		
);

DROP TABLE IF EXISTS usuario;
CREATE TABLE usuario(
	idusuario serial not null,
	nome varchar,
	email varchar,
	login varchar,
	senha varchar,
	is_usuario_crm boolean default true
);

INSERT INTO usuario (nome,email,login,senha) VALUES ('Danilo','danilo@gwsistemas.com.br','suporte','123');
INSERT INTO usuario (nome,email,login,senha) VALUES ('Glauco','glauco@gwsistemas.com.br','suporte2','123');

DROP TABLE IF EXISTS crm_motivos;
CREATE TABLE crm_motivos (
	id serial not null,
	motivo varchar
);

INSERT INTO crm_motivos (motivo) values ('Problema no CT-E');
INSERT INTO crm_motivos (motivo) values ('Problema no Manifesto');


DROP TABLE IF EXISTS crm_motivos;
CREATE TABLE crm_motivos (
	id serial not null,
	motivo varchar,
	sistema varchar
);

INSERT INTO crm_motivos (motivo,sistema) VALUES ('CT-e preso e n�o confirmado','trans');
INSERT INTO crm_motivos (motivo,sistema) VALUES ('CT-e quebrado','trans');
INSERT INTO crm_motivos (motivo,sistema) VALUES ('Manifesto com defeito','trans');
INSERT INTO crm_motivos (motivo,sistema) VALUES ('Ipva em Janeiro','trans');

CREATE TABLE crm_historico_ocorrencia (
	id serial not null,
	idOcorrencia int not null,
	motivo varchar,
	observacao varchar,
	status varchar
);

CREATE SCHEMA gw_chat;
DROP TABLE IF EXISTS conversas_chat;
CREATE TABLE gw_chat.conversas_chat(
	id_ocorrencia int not null,
	conversa text not null,
	data date,
	hora timetz
);

CREATE TABLE gw_chat.config_chat(
	mail_envio_conversa int not null,
	CONSTRAINT mail_envio_conversa_fkey FOREIGN KEY (mail_envio_conversa) REFERENCES config_emails (id) ON UPDATE CASCADE ON DELETE NO ACTION
);


INSERT INTO config_emails(descricao,mail_servidor,mail_remetente,
mail_nome_remetente, mail_smtp_porta,mail_usuario, mail_senha,  is_mail_autenticado, 
is_starttls, is_ssl, is_fatura, 
created_by, created_at, is_oculto) 
VALUES ('INTERNO (BACKUP)','mail.gwsistemas.com.br','backup@gwsistemas.com.br',
'backup_atrasado',587, 'backup@gwsistemas.com.br',
'backup123321','1','1','0','0',1,CURRENT_DATE,true);

DROP TABLE IF EXISTS config_emails;
CREATE TABLE config_emails(
	id serial not null,
	descricao varchar,
	mail_servidor varchar,
	mail_remetente varchar,
	mail_nome_remetente varchar,
	mail_smtp_porta int,
	mail_usuario varchar,
	mail_senha varchar,
	is_mail_autenticado varchar,
	is_starttls varchar,
	is_ssl varchar,
	is_fatura varchar,
	created_by int,
	created_at date,
	is_oculto boolean,
	constraint id primary key(id) 
)