<%-- 
    Document   : index
    Created on : 26/10/2016, 21:55:25
    Author     : Mateus
--%>
<%@page import="java.util.Collection"%>
<%@page import="br.com.gwchat.geral.Suporte"%>
<%
    Suporte suporte = null;
    if (request.getSession(false).getAttribute("suporte") == null) {
        response.sendRedirect("login-suporte");
    } else {
        suporte = (Suporte) request.getSession(false).getAttribute("suporte");
        request.setAttribute("suporte", suporte);
    }

    Collection<String> lista = (Collection) request.getAttribute("listaMotivos");

%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<jsp:include page="/importAlerts.jsp">
    <jsp:param name="caminhoImg" value="img/gw-sistemas.png"/>
    <jsp:param name="nomeUsuario" value="${suporte.nome}"/>
</jsp:include>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Gw Chat</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="css/material.min.css" rel="stylesheet" type="text/css"/>
        <script src="js/material.min.js" type="text/javascript"></script>
        <script src="js/jquery.js" type="text/javascript"></script>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <style>
            .lb-topo-aguardando{
                color: #777;
                font-size: 25px;
                font-weight: bold;
                font-family: "Helvetica","Arial",sans-serif !important;
            }
            .container-status{
                width: 100%;
                float: left;
            }

            .container-infomacoes{
                width: 100%;
                float: left;
                margin-top: 20px;
                /*background: #ccc;*/
                border: 1px solid #ccc;
            }

            .demo-list-control {
                width: 300px;
                margin: 0;
            }

            .demo-list-radio {
                display: inline;
            }

            .status-atual{
                font-family: "Roboto","Helvetica","Arial",sans-serif;
                font-size: 12px;
                margin-left: 10px;
                font-weight: bold;
            }

            .btn-sair{
                float: right;
            }
        </style>
        <script>
            var _0x6a05 = ["\x41\x4C\x54\x45\x52\x41\x52\x5F\x53\x54\x41\x54\x55\x53\x23\x21\x40\x21\x23\x4F\x4E\x4C\x49\x4E\x45", "\x41\x4C\x54\x45\x52\x41\x52\x5F\x53\x54\x41\x54\x55\x53\x23\x21\x40\x21\x23\x4F\x46\x46\x4C\x49\x4E\x45", "\x41\x4C\x54\x45\x52\x41\x52\x5F\x53\x54\x41\x54\x55\x53\x23\x21\x40\x21\x23\x42\x41\x4E\x48\x45\x49\x52\x4F"];
            var protocolo = null;
            var socket = null;
            var loginSuporte = '${suporte.login}';
            var nomeSuporte = '${suporte.nome}';

            <%if (suporte != null) {
                    for (String modulos : suporte.getModulos()) {%>
            var modulo = '<%=modulos%>' + '';
            <%}
                }%>

            function alterarStatus() {
                if (jQuery('.demo-list-control').css('display') === 'none') {
                    jQuery('.demo-list-control').show('fast');
                } else {
                    jQuery('.demo-list-control').hide('fast');
                }
            }

            function conectar(status) {
                if (socket != null && socket != undefined) {
                    socket.close();
                    socket = null;
                }

                if (status != null && status != undefined) {
                    socket = new WebSocket('ws://192.168.15.10:8084/gwChat/chatServer?is_cliente=false&status=' + status + '&modulo=' + modulo + '&login=' + loginSuporte + '&nome=' + nomeSuporte);
                } else {
                    socket = new WebSocket('ws://192.168.15.10:8084/gwChat/chatServer?is_cliente=false&modulo=' + modulo + '&login=' + loginSuporte + '&nome=' + nomeSuporte);
                }
                jQuery('.status-atual span').html(status.toUpperCase());
                socket.onmessage = socketOnMessage;
                socket.onerror = socketOnError;
            }

            function socketOnError(error) {
                console.error('WebSocket Error ' + error);
            }

            var podeContarTempo = true;
            var limparTempo = false;
            var ultimoTempoRegistrado = '';

            function socketOnMessage(e) {
                var txt = e.data.trim();
                javisualizou = false;

                if (txt.indexOf('INYCY0U@T3NDYM3NT0') !== -1) {
                    protocolo = txt.split(':')[1];
                    txt = txt.split(':')[0];
                    jQuery('input[name=status]').attr('disabled', true);
                    jQuery('.gw-chat-B ul').find('li').remove();

                    setInterval(function () {
                        if (limparTempo) {
                            ultimoTempoRegistrado = jQuery('#relogio').val();
                            jQuery('#relogio').val('00:00:00');
                            limparTempo = false;
                        }
                        if (podeContarTempo) {
                            tempo();
                        }
                    }, 1000);

                    return false;
                }

                if (txt == 'D3SC0N3CT@R') {
                    podeContarTempo = false;
                    limparTempo = true;
                    socket.close();
                    socket = null;
                    protocolo = null;
                    jQuery('.container-motivo').css('display', 'block');
                    jQuery('.cobre-tudo').css('display', 'block');


                    return false;
                }

                if (txt == 'VYZUALYS0U') {
                    jQuery('.gw-chat-B-azul img').show(400);
                    return false;
                }



                if (txt.split('!@!')[1] != undefined) {
                    var horaMsg = txt.split('!@!')[0];
                    var nm = txt.split('!@!')[1].split('#@#')[0];
                    var msg = txt.split('!@!')[1].split('#@#')[1];

                    if (jQuery('.gw-chat-A-dados-suporte b').html() == 'Fila de Espera' && nm != nomeSuporte) {
                        jQuery('.gw-chat-A-dados-suporte b').html(nm);
                    }

                    if (nomeSuporte.toUpperCase().trim() == nm.toUpperCase().trim()) {
                        jQuery('.gw-chat-B ul').append('<li><div class="gw-chat-B-azul" style="word-break: break-all;"><span><b>' + horaMsg + ' ' + nm + ':</b></span><br><label>' + msg + '</label><img src="img/visualizado.png"></div></li>');
                    } else {
                        jQuery('.gw-chat-B ul').append('<li><div class="gw-chat-B-cinza" style="word-break: break-all;"><span><b>' + horaMsg + ' ' + nm + ':</b></span><br><label>' + msg + '</label></div></li>');
                    }

                    if (protocolo != null) {
                        jQuery.ajax({
                            url: 'GwChatControlador',
                            data: {
                                acao: 'gravarConversa',
                                conversa: jQuery('.gw-chat-B ul li').last()[0].innerText.replace('\n', ''),
                                protocolo: protocolo
                            },
                            success: function (data, textStatus, jqXHR) {
                            }
                        });
                    }

                } else {
                    jQuery('.gw-chat-B ul').append('<li><div class="gw-chat-B-cinza" style="word-break: break-all;"><span><b>' + horaMsg + ' ' + nm + '</b></span><br><label>' + msg + '</label></div></li>');
                }

                acompanharScroll();
            }


            function digitando() {
                if (jQuery('.gw-chat-B ul li').last().find('div').attr('class') == "gw-chat-B-cinza" && !javisualizou && socket != null) {
                    socket.send("VYZUALYS0U");
                    javisualizou = true;
                }
            }


            function enviarMenssagem(evento, elemento) {
                if (evento.keyCode == '13' && jQuery('#gw-chat-C-chk-enter').prop('checked') == true) {
                    if (socket != null && jQuery('.gw-chat-C-txt-msg').val().trim() != "") {
                        socket.send(jQuery(elemento).val());
                        jQuery(elemento).val('');
                        acompanharScroll();
                    }
                }
            }

            function enviarMenssagemClick() {
                if (socket != null && jQuery('.gw-chat-C-txt-msg').val().trim() != "") {
                    socket.send(jQuery('.gw-chat-C-txt-msg').val());
                    jQuery('.gw-chat-C-txt-msg').val('');
                }
            }

            function acompanharScroll() {
                jQuery('.gw-chat-B').animate({
                    scrollTop: jQuery('.gw-chat-B ul').height()
                }, 500);
            }


            var javisualizou = false;


            jQuery(document).ready(function () {
                jQuery('.gw-chat').hover(
                        function () {
                            if (jQuery('.gw-chat-B ul li').last().find('div').attr('class') == "gw-chat-B-cinza" && !javisualizou && socket != null) {
                                socket.send("VYZUALYS0U");
                                javisualizou = true;
                            }
                        }
                );

                jQuery('[name=status]').change(function () {
                    jQuery('.status-atual span').html(jQuery(this).val());
                    if (jQuery(this).val() == "BANHEIRO") {
                        conectar('banheiro');
                        jQuery('.mdl-layout__header-row').css('background', 'orange');
                        jQuery('.topo-container').css('background', 'orange');
                        acompanharScroll();
                    } else if (jQuery(this).val() == "OFFLINE") {
                        conectar('offline');
                        jQuery('.mdl-layout__header-row').css('background', 'red');
                        jQuery('.topo-container').css('background', 'red');
                        acompanharScroll();
                    } else {
                        conectar('online');
                        jQuery('.mdl-layout__header-row').css('background', '#3b5998');
                        jQuery('.topo-container').css('background', '#3b5998');
                        acompanharScroll();
                    }
                    console.log(socket);
                    salvandoStatus();
                });
                jQuery('.mdl-layout__header-row').css('background', 'red');
                jQuery('.topo-container').css('background', 'red');

                jQuery('.gw-chat-A-maxminizar').click(function () {
                    maxminizarChat();
                });
            });

            function desconectar() {
                jQuery.ajax({
                    url: 'GwChatControlador',
                    type: 'POST',
                    data: {
                        acao: 'logout_suporte'
                    },
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        if (data == 1) {
                            window.location = 'login-suporte';
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Ocorreu um erro ao tentar executar sair.');
                    }
                });
            }



            function salvandoStatus() {
                var status = jQuery('input[name=status]:checked').val();
                if (socket != null) {
                    if (status == 'ONLINE') {
                        socket.send(_0x6a05[0]);
                    } else if (status == 'OFFLINE') {
                        socket.send(_0x6a05[1]);
                    } else if (status == 'BANHEIRO') {
                        socket.send(_0x6a05[2]);
                    }

                }

            }


            var segundo = 0 + "0";
            var minuto = 0 + "0";
            var hora = 0 + "0";

            function tempo() {
                if (segundo < 59) {
                    segundo++
                    if (segundo < 10) {
                        segundo = "0" + segundo
                    }
                } else
                if (segundo == 59 && minuto < 59) {
                    segundo = 0 + "0";
                    minuto++;
                    if (minuto < 10) {
                        minuto = "0" + minuto
                    }
                }
                if (minuto == 59 && segundo == 59 && hora < 23) {
                    segundo = 0 + "0";
                    minuto = 0 + "0";
                    hora++;
                    if (hora < 10) {
                        hora = "0" + hora
                    }
                } else
                if (minuto == 59 && segundo == 59 && hora == 23) {
                    segundo = 0 + "0";
                    minuto = 0 + "0";
                    hora = 0 + "0";
                }

                jQuery('#relogio').val(hora + ":" + minuto + ":" + segundo);

            }

            function maxminizarChat() {
                if (jQuery('.gw-chat').css('width') != '500px') {
                    maxminizar();
                    jQuery('.gw-chat').css('width', '500px');
                    jQuery('.gw-chat').css('height', '450px');
                    jQuery('.gw-chat-C').css('height', '115px');
                    jQuery('.gw-chat-C-txt-msg').css('height', '60px');
                    jQuery('.gw-chat-C-btn-enviar-msg').css('margin-left', '20px');
                    jQuery('.gw-chat-C-lb-chk-enter').css('margin-right', '118px');
                } else {
                    restaurar();
                    jQuery('.gw-chat').css('width', '290px');
                    jQuery('.gw-chat').css('height', '410px');
                    jQuery('.gw-chat-C').css('height', '82px');
                    jQuery('.gw-chat-C-txt-msg').css('height', '30px');
                    jQuery('.gw-chat-C-btn-enviar-msg').css('margin-left', '10px');
                    jQuery('.gw-chat-C-lb-chk-enter').css('margin-right', '26px');
                }
            }

            function abrir() {
                if (document.getElementById('iframeChat').style.height === '410px') {
                    jQuery(document.getElementById('iframeChat')).animate({
                        'height': '28px'
                    });
                } else {
                    jQuery(document.getElementById('iframeChat')).animate({
                        'height': '410px'
                    });
                }
            }

            function isAberto() {
                if (document.getElementById('iframeChat').style.height === '410px' || document.getElementById('iframeChat').style.height === '450px') {
                    return true;
                } else {
                    return false;
                }
            }

            function isMaxminizado() {
                if (document.getElementById('iframeChat').style.height === '450px') {
                    return true;
                } else {
                    return false;
                }
            }

            function minimizar() {
                jQuery(document.getElementById('iframeChat')).animate({
                    'height': '28px',
                    'width': '290px'
                });
            }

            function maxminizar() {
                jQuery(document.getElementById('iframeChat')).animate({
                    'width': '500px'
                }, 300, function () {
                    jQuery(document.getElementById('iframeChat')).animate({
                        'height': '450px'
                    });
                });
            }

            function restaurar() {
                jQuery(document.getElementById('iframeChat')).animate({
                    'width': '290px'
                }, 300, function () {
                    jQuery(document.getElementById('iframeChat')).animate({
                        'height': '410px'
                    });
                });
            }
        </script>
    </head>
    <body onload="conectar('offline');">
        <label style="position: absolute;right: 10%;bottom: 360px;z-index: 99999;font-size: 10px;margin-right: 10px;font-weight: bold;color: #777;">Tempo de atendimento</label>
        <input type="text" disabled="true" value="00:00:00" name="relogio" id="relogio" size="10" style="position: absolute;bottom: 345px;width: 50px;margin-right: 10px;font-size: 11px;right: 10%;z-index: 99999;background: #fff;border: 0px;">
        <div class="demo-layout-waterfall mdl-layout mdl-js-layout">
            <header class="mdl-layout__header mdl-layout__header--waterfall">
                <div class="mdl-layout__header-row">
                    <!-- Title -->
                    <span class="mdl-layout-title">Gw Chat</span>
                </div>
            </header>
            <div class="mdl-layout__drawer">
                <span class="mdl-layout-title">Gw Chat</span>
                <nav class="mdl-navigation">
                    <a class="mdl-navigation__link" href="#" onclick="desconectar();">Sair</a>
                </nav>
            </div>
            <main class="mdl-layout__content">
                <div class="page-content" >
                    <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect" >
                        <div class="mdl-tabs__tab-bar">
                            <a href="#fila-espera" class="mdl-tabs__tab is-active">Suporte - Gw Chat</a>
                        </div>
                        <div class="mdl-tabs__panel is-active" id="fila-espera">
                            <div style="width: 100%;background: #e9ebee;">
                                <div class="mdl-grid">
                                    <div class="mdl-cell mdl-cell--6-col">
                                        <div class="container-serie" style="float: left;">
                                            <div style="padding: 15px;width: 100%;background: #fff;border-radius: 5px;float: left;">
                                                <center><label class="lb-topo-aguardando">Informa��es</label></center>
                                                <br>

                                                <div class="container-status">
                                                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" onclick="alterarStatus();">
                                                        Alterar Status
                                                    </button>
                                                    <label class="status-atual">STATUS ATUAL: <span>DESCONECTADO</span><test></test></label>
                                                    <!-- List with avatar and controls -->
                                                    <ul class="demo-list-control mdl-list">
                                                        <li class="mdl-list__item">
                                                            <span class="mdl-list__item-primary-content">
                                                                <i class="material-icons  mdl-list__item-avatar" style="background: #3b5998;">person</i>
                                                                Online
                                                            </span>
                                                            <span class="mdl-list__item-secondary-action">
                                                                <label class="demo-list-radio mdl-radio mdl-js-radio mdl-js-ripple-effect" for="list-option-1">
                                                                    <input type="radio" id="list-option-1" class="mdl-radio__button" name="status" value="ONLINE" />
                                                                </label>
                                                            </span>
                                                        </li>
                                                        <li class="mdl-list__item">
                                                            <span class="mdl-list__item-primary-content">
                                                                <i class="material-icons  mdl-list__item-avatar" style="background: orange;">person</i>
                                                                Banheiro
                                                            </span>
                                                            <span class="mdl-list__item-secondary-action">
                                                                <label class="demo-list-radio mdl-radio mdl-js-radio mdl-js-ripple-effect" for="list-option-2">
                                                                    <input type="radio" id="list-option-2" class="mdl-radio__button" name="status" value="BANHEIRO" />
                                                                </label>
                                                            </span>
                                                        </li>
                                                        <li class="mdl-list__item">
                                                            <span class="mdl-list__item-primary-content">
                                                                <i class="material-icons  mdl-list__item-avatar" style="background: red;">person</i>
                                                                Offline
                                                            </span>
                                                            <span class="mdl-list__item-secondary-action">
                                                                <label class="demo-list-radio mdl-radio mdl-js-radio mdl-js-ripple-effect" for="list-option-3">
                                                                    <input type="radio" id="list-option-3" class="mdl-radio__button" name="status" value="OFFLINE" checked/>
                                                                </label>
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="container-infomacoes">
                                                    <style>
                                                        .topo-cont-info{
                                                            width: 100%;
                                                            float: left;
                                                            margin-bottom: 10px;
                                                            margin-top: 10px;
                                                            color: #666;
                                                            font-weight: bold;
                                                            font-size: 16px;
                                                        }

                                                        .corpo-cont-info{
                                                            width: 100%;
                                                            float: left;
                                                            margin-bottom: 20px;
                                                        }
                                                    </style>
                                                    <div class="topo-cont-info">
                                                        <center>
                                                            <label>Modulos</label>
                                                        </center>
                                                    </div>
                                                    <div class="corpo-cont-info">
                                                        <center>
                                                            <%if (suporte != null) {
                                                                    for (String modulos : suporte.getModulos()) {%>  
                                                            <img src="img/<%=modulos%>_footer.png">
                                                            <%}
                                                                }%>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </main>
        </div>
        <div class="gw-chat" style="bottom: 0px;position: absolute;z-index: 9999;float: left;right: 10%;box-shadow: 0px 0px 1px 1px rgba(0,0,0,0.3);">
            <div class="gw-chat-A">
                <div class="gw-chat-A-topo">
                    <center>Gw Chat - Suporte</center>
                </div>
                <div class="gw-chat-A-buttons">
                    <div class="gw-chat-A-fechar" style="display: none;"></div>
                    <div class="gw-chat-A-maxminizar" style="margin-right: 5px;"></div>
                    <div class="gw-chat-A-minimizar" style="display: none;"></div>
                </div>
                <div class="gw-img-perfil-suporte">
                    <img src="img/icone-gw.png">
                </div>
                <div class="gw-chat-A-dados-suporte">
                    <span><b>Fila de Espera</b></span>
                    <label>Suporte ao cliente</label>
                </div>
            </div>
            <div class="gw-chat-B">
                <ul style="width: 100%;">
                </ul>
            </div>
            <div class="gw-chat-B-question">
                <center style="margin-top: 10px;float: left;width: 100%;font-size: 17px;font-weight: bold;color: #666;">
                    Avalia��o do atendente
                </center>
                <center style="float: left;margin-top: 10px;width: 100%;">
                    <div style="width: 100%;float: left;">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width: 90%;">
                            <input class="mdl-textfield__input" type="text" id="observacao" name="observacao" style="color: #0d2d4a;">
                            <label class="mdl-textfield__label" for="observacao" style="">Observa��o sobre o atendimento</label>
                        </div>
                    </div>
                    <div style="width: 100%;float: left;margin-top: 15px;">
                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="gwTrans">
                            <input type="radio" id="gwTrans" class="mdl-radio__button" name="options" value="1" checked>
                            <span class="mdl-radio__label" style="font-size: 11px;">Bom</span>
                        </label>
                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="gwLogis">
                            <input type="radio" id="gwLogis" class="mdl-radio__button" name="options" value="2">
                            <span class="mdl-radio__label" style="font-size: 11px;">M�dio</span>
                        </label>
                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="gwFrota">
                            <input type="radio" id="gwFrota" class="mdl-radio__button" name="options" value="2">
                            <span class="mdl-radio__label" style="font-size: 11px;">Ruim</span>
                        </label>
                    </div>
                </center>
                <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-1" style="margin-left: 59px;margin-top: 10px;">
                    <input type="checkbox" id="checkbox-1" class="mdl-checkbox__input">
                    <span class="mdl-checkbox__label" style="font-size: 9px;">C�pia do atendimento por e-mail</span>
                </label>
                <center style="float: left;margin-top: 10px;width: 100%;">
                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="background: #0d2d4a;color: #fff;font-size: 10px !important;" onclick="finalizar();">
                        Finalizar
                    </button>
                </center>
            </div>
            <div class="gw-chat-C">
                <center>
                    <!--<input type="tex" placeholder="Escreva aqui sua mensagem">-->
                    <textarea placeholder="Escreva aqui sua mensagem" class="gw-chat-C-txt-msg" onkeyup="enviarMenssagem(event, this);
                            digitando();"></textarea>
                </center>
                <center>
                    <input type="button" class="gw-chat-C-btn-enviar-msg" value="Enviar Mensagem" onclick="enviarMenssagemClick();">
                </center>
                <input type="checkbox" class="gw-chat-C-chk-enter" id="gw-chat-C-chk-enter" checked><label class="gw-chat-C-lb-chk-enter" for="gw-chat-C-chk-enter">Enter para enviar</label>
            </div>
        </div>
        <style>
            .cobre-tudo{
                display: none;
                width: 100%;
                height: 100%;
                background: rgba(0,0,0,0.6);
                z-index: 9999999;
                top: 0;
                left: 0;
                position: absolute;
            }
            .container-motivo{
                display: none;
                z-index: 99999999;
                width: 300px;
                height: 235px;
                position: absolute;
                background: #fff;
                top: 50%;
                left: 50%;
                margin-left: -150px;
                margin-top: -100px;
                border-radius: 5px;
                -webkit-box-shadow: 0px 0px 2px 2px rgba(50, 50, 50, 0.75);
                -moz-box-shadow:    0px 0px 2px 2px rgba(50, 50, 50, 0.75);
                box-shadow:         0px 0px 2px 2px rgba(50, 50, 50, 0.75);
            }

            .container-motivo-topo{
                font-size: 16px;
                font-weight: bold;
                background: #0d2d4a;
                color: #fff;
                padding-top: 10px;
                padding-bottom: 10px;
            }
        </style>
        <div class="cobre-tudo">
        </div>
        <div class="container-motivo">
            <center class="container-motivo-topo">Motivo da ocorr�ncia</center>
            <center>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth" style="width: 90%;">
                    <input class="mdl-textfield__input" type="text" id="motivo" value="Nenhum motivo selecionado" readonly tabIndex="-1">
                    <label for="motivo" class="mdl-textfield__label">Selecione um motivo</label>
                    <ul for="motivo" class="mdl-menu mdl-menu--bottom-left mdl-js-menu" id="ul-motivos">
                        <%--<c:forEach var="motivo" varStatus="status" items="${listaMotivos}">--%>
                        <%--<c:out escapeXml="true" value="${motivo}" />--%>
                        <%--</c:forEach>--%>
                        <%if (lista != null) {
                                for (String motivo : lista) {%>
                        <li class="mdl-menu__item"><%=motivo%></li>
                            <%}%>
                            <%}%>
                    </ul>
                </div>
            </center>
            <center>
                <div class="mdl-textfield mdl-js-textfield" style="width: 90%;padding: 5px 0;">
                    <textarea class="mdl-textfield__input" type="text" rows= "3" id="observacaoOcorrencia" ></textarea>
                    <label class="mdl-textfield__label" for="observacaoOcorrencia" style="bottom: -16px !important;top: 8px !important;">Observa��o...</label>
                </div>
            </center>
            <center>
                <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" onclick="salvarMotivoOcorrencia();" style="background: #0d2d4a;font-size: 11px;">
                    Salvar
                </button>
            </center>
        </div>
        <script>
            function salvarMotivoOcorrencia() {
                if (jQuery('#motivo').val() == "Nenhum motivo selecionado") {
                    chamarAlert('O campo motivo � obrigat�rio.');
                    return false;
                }
                if (jQuery('#observacaoOcorrencia').val() == "") {
                    chamarAlert('O campo observa��o � obrigat�rio.');
                    jQuery('#observacaoOcorrencia').focus();
                    return false;
                }


                var dt = new Date();

                if (socket != null) {
                    socket.close();
                    socket = null;
                }

                jQuery('.gw-chat-B ul').append('<li><div class="gw-chat-B-cinza" style="word-break: break-all;"><span><b>' + dt.getHours() + ':' + dt.getMinutes() + 'GW Chat</b></span><br><label>Voc� tem 15 segundos para alterar seu status antes de entrar na fila de atendimento.</label></div></li>');
                acompanharScroll();

                jQuery('input[name=status]').attr('disabled', false);

                jQuery.ajax({
                    url: 'GwChatControlador',
                    data: {
                        acao: 'gravarMotivoOcorrencia',
                        motivo: jQuery('#motivo').val(),
                        observacao: jQuery('#observacaoOcorrencia').val(),
                        protocolo: protocolo
                    },
                    success: function (data, textStatus, jqXHR) {
                    }
                });
                jQuery('.cobre-tudo').hide();
                jQuery('.container-motivo').hide();

                jQuery('#motivo').val('Nenhum motivo selecionado');
                jQuery('#observacaoOcorrencia').val('');


                setTimeout(function () {
                    if (jQuery('input[name=status]:checked').val() == 'ONLINE') {
                        conectar('online');
                    }
                }, 15000);

            }

            function getConversa() {
                var index = 0;
                var conversa = '';
                while (jQuery('.gw-chat-B ul li')[index] != undefined) {
                    conversa += jQuery('.gw-chat-B ul li')[index].innerText + '#@#';
                    index++;
                }

                return conversa.replace('\n', '');
            }

        </script>
    </body>
</html>
