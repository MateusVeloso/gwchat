<%-- 
    Document   : cliente
    Created on : 28/10/2016, 00:36:18
    Author     : Mateus
--%>

<%@page import="br.com.gwchat.geral.Cliente"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <script src="js/jquery.js" type="text/javascript"></script>
        <script src="js/material.min.js" type="text/javascript"></script>
        <link href="css/material.min.css" rel="stylesheet" type="text/css"/>


        <title>JSP Page</title>
        <!--<embed src="http://192.168.124:8080/gwChat/cliente" style="position: absolute;bottom: 0;right: 10%;z-index: 9999999999;width: 350px;height: 400px;">-->
        <style>
            html,body{
                margin: 0;
                padding: 0;
                overflow: hidden !important;
            }
            .container-chat{
                height: 320px;
                width: 260px;
                background: #fff;
                overflow: hidden;
                border: 0.5px solid rgba(0,0,0,0.2);
            }
            .topo-container{
                border-radius: 7px 7px 0 0;
                width: 100%;
                float: left;
                padding-top: 4px;
                padding-bottom: 4px;
                padding-left: 10px;
                background: rgba(19,59,92,1);
                cursor: pointer;
                /*margin-bottom: 5px;*/
            }
            .topo-container:hover{
                background: rgba(19,59,92,0.9);
            }
            .lb-topo{
                margin-top: 7px;
                width: 80%;
                float: left;
                color: #fff;
                font-family: inherit;
                font-weight: bold;
                font-size: 13px;
                /*font-family: "Helvetica","Arial",sans-serif !important;*/
            }

            .corpo-container{
                width: 100%;
                float: left;
                height: 220px;
                overflow-y: scroll;
                overflow-x: hidden;
                /*background: #fff;*/
            }

            .footer-container{
                overflow: hidden;
                width: 100%;
                height: 60px;
                /*background: #ccc;*/
                border-top: 1px solid #ccc;
                float: left;
            }

            .footer-container textarea{
                resize: none;
                width: 100%;
                height: 60px;
                font-family: "Helvetica","Arial",sans-serif !important;
                color: #666;
                font-size: 16px;
                /*font-weight: bold;*/
            }

            .cobre-div-cinza-azul{
                width: 100%;
                float: left;
            }

            .div-cinza{
                font-family: inherit !important;
                font-size: 12px;
                word-break: break-all;
                /*font-weight: bold;*/
                background: #e9ebee;
                color: #4b4f56;
                max-width: 210px;
                float: left;
                border-radius: 0 7px 7px 0;
                padding: 5px;
                margin-bottom: 1px;
                /*white-space: pre-wrap;*/
            }

            .div-azul{
                font-family: inherit !important;
                font-size: 12px;
                word-break: break-all;
                /*font-weight: bold;*/
                background: #1c66a6;
                color: #fff;
                max-width: 210px;
                float: right;
                border-radius: 7px 0px 0px 7px;
                padding: 5px;
                margin-bottom: 1px;
                /*white-space: pre-wrap;*/
            }

            .td-azul{
                font-family: inherit !important;
                font-size: 12px;
                /*font-weight: bold;*/
                background: #e9ebee;
                color: #4b4f56;
                max-width: 210px;
                float: right;
                border-radius: 7px 0px 0px 7px;
                padding: 5px;
                margin-bottom: 1px;
                /*white-space: pre-wrap;*/
            }
        </style>
        <script>

            jQuery(document).ready(function () {
                jQuery('.container-chat').click(function () {
                    document.getElementById('menssagem').focus();
                    socket.send('Y� b�i');
                });
            });

            function acompanharScroll() {
                jQuery('.corpo-container').animate({
                    scrollTop: jQuery('#divMenssagens').height()
                }, 500);
            }

            var socket = null;
            <%
                Cliente c = (Cliente) request.getAttribute("cliente");
                if (c == null) {
                    c = new Cliente("a", "aa", "aa", "frota");
                }
            %>
            var cnpj = '<%=c.getCnpj()%>';
            var nome = '<%=c.getNome()%>';
            var email = '<%=c.getEmail()%>';
            var modulo = '<%=c.getModulo()%>';

            function conectarCliente() {
                jQuery('#menssagem').removeAttr('disabled');
                socket = new WebSocket('ws://192.168.0.104:8084/gwChat/chatServerCliente?nome_usuario=' + nome + '&cnpj=' + cnpj + '&email=' + email + '&modulo=' + modulo);
                socket.onmessage = socketOnMessage;
                socket.onclose = socketOnClose;
                socket.onerror = socketOnError;
            }

            function socketOnError(error) {
                console.log('WebSocket Error ' + error);
            }

            function socketOnMessage(e) {
                var txt = e.data;

                if (txt == 'Y� b�i') {
                    if (jQuery('#menssagensVisualizadas').css('display') == 'none' && jQuery('#divMenssagens').find('.cobre-div-cinza-azul').last().find('div')[0].className == 'div-azul') {
                        jQuery('#menssagensVisualizadas').css('display', 'block');
                    }
                    return false;
                }

                var nm = txt.split("#@#")[0];
                nm = nm.split("!@!")[1];

                txt = txt.replace('!@!', ' ');
                txt = txt.replace('#@#', ': ');

                if (jQuery('#menssagensVisualizadas').css('display') == 'block') {
                    jQuery('#menssagensVisualizadas').css('display', 'none');
                }

                if (nm != undefined && nm.toUpperCase() == nome.toUpperCase()) {
                    jQuery('#divMenssagens').append('<div class="cobre-div-cinza-azul"><div class="div-azul">' + txt + '</div></div>');
                    acompanharScroll();
                    jQuery('.cobre-container').hide();
                    jQuery('#btDesconectar').attr('disabled', false);
                } else {
                    jQuery('#divMenssagens').append('<div class="cobre-div-cinza-azul"><div class="div-cinza">' + txt + '</div></div>');
                    acompanharScroll();
                    jQuery('.cobre-container').hide();
                }
            }


            function socketOnClose(e) {
                jQuery('#divMenssagens').hide('slow');
                jQuery('.cobre-container').show('slow');
                jQuery('#btDesconectar').attr('disabled', 'true');
                jQuery('#menssagem').attr('disabled', 'true');
            }

            function desconectar() {
                var r = confirm("Tem certeza que deseja encerrar esse atendimento?");
                if (r == true) {
                    socket.close();
                    jQuery('.corpo-container').css('height', '292px');
                    jQuery('.cobre-container').css('height', '292px');
                    jQuery('.footer-container').css('display', 'none');

                } else {
                    return false;
                }
            }

            window.onbeforeunload = function () {
                return 'Tem certeza que deseja encerrar esse atendimento? Se sim, o atendimento ser� encerrado automaticamente.';
            };

            function enviarMenssagem(evento, elemento) {
                if (jQuery('#menssagensVisualizadas').css('display') == 'block') {
                    jQuery('#menssagensVisualizadas').css('display', 'none');
                }

                if (evento.keyCode == '13') {
                    socket.send(jQuery(elemento).val());
                    jQuery(elemento).val('');
                }
            }

            function novoChamado() {
                window.location = "loginCliente.jsp";
            }

            function enviarConversaEmail() {

            }
        </script>
    </head>
    <body onload="conectarCliente();">
        <div class="container-chat">
            <div class="topo-container">
                <div class="lb-topo">GW Chat - Suporte</div>
                <button id="demo-menu-lower-right"
                        class="mdl-button mdl-js-button mdl-button--icon">
                    <i class="material-icons" style="color: #fff;">more_vert</i>
                </button>

                <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                    for="demo-menu-lower-right"  style="">
                    <li class="mdl-menu__item" onclick="desconectar();" style="">Encerrar atendimento</li>
                </ul>
            </div>
            <div class="corpo-container">
                <style>
                    .cobre-container{
                        display: none;
                        width: 100%;
                        height: 220px;
                        background: rgba(0,0,0,0.1);
                    }

                    .cobre-container label{
                        font-size: 12px;
                        font-weight: bold;
                        color: #666;
                    }

                    .div-btn-finalizar{
                        width: 90%;
                        border-radius: 5px;
                        height: 28px;
                        color:#fff;
                        background: rgba(19,59,92,1);
                        cursor: pointer;
                    }

                    .div-btn-finalizar:hover{
                        -webkit-box-shadow: 0px 0px 1px 1px rgba(0,0,0,0.3);
                        -moz-box-shadow: 0px 0px 1px 1px rgba(0,0,0,0.3);
                        box-shadow: 0px 0px 1px 1px rgba(0,0,0,0.3);
                        background: rgba(19,59,92,0.9);
                    }

                    .div-btn-finalizar center{
                        float: left;
                        text-align: center;
                        width: 100%;
                        margin-top: 4px;
                        cursor: pointer;
                    }

                    .inputObservacao{
                        width: 80%;
                        border-radius: 5px;
                        font-size: 9px;
                        padding: 5px;
                    }

                    .inputObservacao:focus{
                        outline: none;
                    }

                    input:-webkit-autofill 
                    {    
                        -webkit-box-shadow: 0 0 0px 1000px #f9fbfd inset !important;
                        -webkit-text-fill-color: rgba(19,59,92,1) !important;
                    }


                </style>
                <div class="cobre-container">
                    <center style="padding-top: 10px;">
                        <label>Avalia��o do atendente</label>
                    </center>
                    <center style="margin-top: 10px;">
                        <input style="" class="inputObservacao" id="observacao" placeholder="Alguma observa��o sobre o atendimento?">
                    </center>
                    <center>
                        <div style="padding: 15px;">
                            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="gwTrans">
                                <input type="radio" id="gwTrans" class="mdl-radio__button" name="options" value="1" checked>
                                <span class="mdl-radio__label" style="font-size: 11px;">Bom</span>
                            </label>
                            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="gwLogis">
                                <input type="radio" id="gwLogis" class="mdl-radio__button" name="options" value="2">
                                <span class="mdl-radio__label" style="font-size: 11px;">M�dio</span>
                            </label>
                            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="gwFrota">
                                <input type="radio" id="gwFrota" class="mdl-radio__button" name="options" value="2">
                                <span class="mdl-radio__label" style="font-size: 11px;">Ruim</span>
                            </label>
                        </div>
                    </center>
                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-1" style="margin-left: 13px;">
                        <input type="checkbox" id="checkbox-1" class="mdl-checkbox__input">
                        <span class="mdl-checkbox__label" style="font-size: 9px;">C�pia do atendimento por e-mail</span>
                    </label>
                    <div style="width: 100%;margin-top: 10px;">
                        <center>
                            <div class="div-btn-finalizar" id="bt-finalizar" onclick="novoChamado();" >
                                <center>Finalizar</center>
                            </div>
                        </center>
                    </div>
                </div>
                <div id="divMenssagens" style="line-height: 13px;float: left;">
                </div>
            </div>
            <div class="footer-container">
                <style>
                    .footer-container input{
                        width: 250px;
                        max-width: 250px;
                        float: left;
                        border: 0px;
                        padding: 5px;
                        font-size: 12px;
                        color: #4b4f56;
                        font-family: inherit;
                        height: 22px;
                    }

                    .footer-container input:focus{
                        outline: none;
                    }

                    .menu-mensagem{
                        /*display: none;*/
                        width: 250px;
                        max-width: 250px;
                        float: left;
                        border: 0px;
                        padding: 5px;
                    }

                    .div-btn{
                        width: 55%;
                        float: right;
                        margin-right: 2%;
                        border-radius: 5px;
                        color:#fff;
                        font-size: 12px;
                        background: rgba(19,59,92,1);
                        cursor: pointer;
                    }
                    .div-btn:hover{
                        -webkit-box-shadow: 0px 0px 1px 1px rgba(0,0,0,0.3);
                        -moz-box-shadow: 0px 0px 1px 1px rgba(0,0,0,0.3);
                        box-shadow: 0px 0px 1px 1px rgba(0,0,0,0.3);
                        background: rgba(19,59,92,0.9);
                    }

                    .div-btn span{
                        float: left;
                        text-align: center;
                        width: 100%;
                        cursor: pointer;
                    }

                    .menu-mensagem-visualizadas{
                        width: 250px;
                        max-width: 250px;
                        float: left;
                        border: 0px;
                    }

                    .div-btn-visualizadas{
                        width: 60%;
                        color:#999;
                        font-size: 10px;
                        /*background: rgba(180,180,180,0.3);*/
                    }

                    .div-btn-visualizadas span{
                        text-align: center;
                        width: 100%;
                    }
                </style>
                <center id="menssagensVisualizadas" style="display: none;">
                    <div class="menu-mensagem-visualizadas">
                        <div class="div-btn-visualizadas">
                            <span>Mensagens visualizadas</span>
                        </div>
                    </div>
                </center>
                <input placeholder="Digite uma mensagem" id="menssagem" name="menssagem"  onkeyup="enviarMenssagem(event, this);">
                <!--                <div class="menu-mensagem">
                                    <div class="div-btn" onclick="desconectar();">
                                        <span>Encerrar atendimento</span>
                                    </div>
                                </div>-->
                <!--<center><textarea placeholder="Digite uma mensagem" id="menssagem" name="menssagem" disabled="true" onkeyup="enviarMenssagem(event, this);"></textarea></center>-->
            </div>
        </div>
    </body>
</html>
