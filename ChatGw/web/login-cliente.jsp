<%-- 
    Document   : login-cliente
    Created on : 27/11/2016, 15:24:59
    Author     : 30porcento
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <title>GW Chat - Login Cliente</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <script src="js/jquery.js" type="text/javascript"></script>
        <script src="js/material.min.js" type="text/javascript"></script>
        <link href="css/material.min.css" rel="stylesheet" type="text/css"/>
        <style>
            .gw-chat-login-cliente-geral{
                width: 290px;
                height: 410px;
                /*background: #ccc;*/
            }

            .gw-chat-login-cliente-geral-topo-A{
                background: #0d2d4a;
                color: #fff;
                font-size: 15px;
                font-weight: 600;
                font-family: 'Lato',sans-serif !important;
                padding-top: 7px;
                padding-bottom: 7px;
                width: 100%;
                cursor: pointer;
                float: left;
            }
            .gw-chat-login-cliente-geral-topo-B{
                width: 100%;
                float: left;
                border-bottom: 1px solid #ccc;
                padding-top: 15px;
                padding-bottom: 15px;
                font-size: 20px;
                color: #707070;
                /*font-family: 'Lato',sans-serif;*/
                /*height: 25px;*/
            }

            .gw-chat-login-cliente-geral-corpo-A{
                width: 100%;
                float: left;
                border-bottom: 1px solid #ccc;
                height: 300px;
                background: #fafafa;
            }

            .gw-chat-login-cliente-geral-corpo-A-autenticacao{
                display: none;
                width: 100%;
                float: left;
                border-bottom: 1px solid #ccc;
                height: 300px;
                background: #fafafa;
            }

            .gw-chat-login-cliente-geral-corpo-A-autenticacao-2{
                display: none;
                width: 100%;
                float: left;
                border-bottom: 1px solid #ccc;
                height: 280px;
                overflow-y: auto;
                overflow-x: hidden;
                background: #fafafa;
            }

            .gw-chat-login-cliente-geral-corpo-B{
                display: none;
                width: 100%;
                float: left;
                height: 43px;
                padding-top: 5px;
                background: #fff;
            }

            .gw-chat-login-cliente-geral-corpo-A-mg-boas-vindas{
                margin-left: 10px;
                margin-top: 10px;
                line-height: 10px;
                font-family: 'Lato',sans-serif;
                font-size: 12px;
                width: 100%;
                float: left;
            }

            .gw-chat-login-cliente-geral-corpo-A-mg-boas-vindas b{
                font-size: 18px;
                font-family: 'Lato',sans-serif;
            }

            .gw-chat-login-cliente-geral-corpo-A-opcoes{
                width: 97%;
                float: left;
                padding-top: 20px;
            }

            .gw-chat-login-cliente-geral-corpo-A-opcoes ul{
                padding: 0px;
                margin: 0px;
                margin-left: 15px;
                list-style: none;
            }

            .gw-chat-login-cliente-geral-corpo-A-opcoes li{
                font-size: 15px;
                font-weight: bold;
                color: #707070;
                padding-top: 7px;
                padding-bottom: 7px;
                border-radius: 5px;
                margin-bottom: 5px;
                background: rgba(238,238,238,0.5);
            }
            .gw-chat-login-cliente-geral-corpo-A-opcoes label{
                margin-left: 10px;
            }
            .gw-chat-login-cliente-geral-corpo-A-opcoes div{
                font-size: 11px;
                float: right;
                background: #666;
                border-radius: 5px;
                margin-right: 5px;
                padding-left: 5px;
                padding-right: 5px;
                color: #fff;
                cursor: pointer;
            }

            .gw-chat-login-cliente-geral-corpo-A-opcoes li:hover{
                background: rgba(238,238,238,1);
            }

            .mdl-textfield__label:after{
                background: #0d2d4a;
            }

            .lb-radio > input{
                visibility: hidden; 
                position: absolute;
            }
            .lb-radio > input + img{
                cursor:pointer;
                border:2px solid transparent;
            }
            .lb-radio > input:checked + img{
                border:2px solid rgba(66,103,178,0.5);
            }

            .lb-radio img{
                width: 70px;
            }

            input:-webkit-autofill 
            {    
                -webkit-box-shadow: 0 0 0px 1000px #f9fbfd inset !important;
                -webkit-text-fill-color: rgba(19,59,92,1) !important;
            }

        </style>
        <script>
            function iniciarSuporte() {
                jQuery('.gw-chat-login-cliente-geral-corpo-A').hide(400);
                setTimeout(function () {
                    jQuery('.gw-chat-login-cliente-geral-corpo-A-autenticacao').show(400);
                }, 400);
            }

            function validar() {
                if (jQuery('#cnpjSerial').val().trim() == '') {
                    parent.chamarAlert('Informe um cnpj ou serial v�lido.');
                    return false;
                }
                
                jQuery.ajax({
                    url: 'GwChatControlador?acao=existeCnpjSerial&cnpjSerial=' + jQuery('#cnpjSerial').val(),
                    success: function (data, textStatus, jqXHR) {

                        if (data.split('!@!')[0] == 'cliente encontrado') {
                            jQuery('#idCliente').val(data.split('!@!')[1]);
                            jQuery('.gw-chat-login-cliente-geral-corpo-A-autenticacao').hide(400);
                            jQuery('.gw-chat-login-cliente-geral-corpo-A-autenticacao-2').show(400);
                            jQuery('.gw-chat-login-cliente-geral-corpo-B').show(400);
//                            var i = 0;
//                            while (data.split('!@!')[i] != null && data.split('!@!')[i] != undefined && data.split('!@!')[i].trim() != '') {
//                                jQuery('input[name=modulo][value=' + data.split('!@!')[i] + ']').parent('.lb-radio').show();
//                                i++;
//                            }

                        } else {
                            parent.chamarAlert(data.split('!@!')[0]);
                        }
                    }
                });
            }


            jQuery(document).ready(function () {
                jQuery('.gw-chat-login-cliente-geral-topo-A').click(function () {
                    parent.abrir();
                });
            });

            function conectar() {
                if (jQuery('#nome').val().trim() == '') {
                    parent.chamarAlert('Informe um nome.');
                    return false;
                }

                if (jQuery('#email').val().trim() == '') {
                    parent.chamarAlert('Informe um e-mail.');
                    return false;
                }

                if (jQuery('input[name=modulo]:checked').val() == undefined) {
                    parent.chamarAlert('Selecione um modulo para seu atendimento.');
                    return false;
                }

                var form = document.getElementById("form");

                form.appendChild(document.getElementById("cnpjSerial"));
                form.appendChild(document.getElementById("nome"));
                form.appendChild(document.getElementById("email"));
                form.appendChild(document.getElementById("motivo"));
                form.appendChild(document.getElementById("idCliente"));

                var i = 0;
                while (document.getElementsByName('modulo')[i] != null) {
                    form.appendChild(document.getElementsByName('modulo')[i]);
                    i++;
                }
                form.submit();

            }
            
            function enviarEnter(event,elemento){
                if (event.keyCode == '13') {
                    validar();
                }
            }
        </script>
    </head>
    <body>
        <div class="gw-chat-login-cliente-geral">
            <div class="gw-chat-login-cliente-geral-topo-A">
                <center>GW Chat</center>
            </div>
            <div class="gw-chat-login-cliente-geral-topo-B">
                <center>Atendimento On-line</center>
            </div>
            <div class="gw-chat-login-cliente-geral-corpo-A">
                <div class="gw-chat-login-cliente-geral-corpo-A-mg-boas-vindas">
                    <table><tr><td><b>Ol�</b></td></tr><tr><td>Como posso te ajudar?</td></tr></table>
                </div>
                <div class="gw-chat-login-cliente-geral-corpo-A-opcoes">
                    <ul>
                        <li>
                            <label>Comercial</label><div>Em manuten��o</div>
                        </li>
                        <li>
                            <label>Financeiro</label><div>Em manuten��o</div>
                        </li>
                        <li>
                            <label>Suporte t�cnico</label><div onclick="iniciarSuporte();">Iniciar atendimento</div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="gw-chat-login-cliente-geral-corpo-A-autenticacao">
                <center>
                    <div style="width: 100%;margin-top: 50px;">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width: 90%;">
                            <input class="mdl-textfield__input" type="text" id="cnpjSerial" name="cnpjSerial" onkeyup="enviarEnter(event,this);" style="color: #0d2d4a;">
                            <label class="mdl-textfield__label" for="cnpjSerial">Cnpj ou Serial</label>
                        </div>
                        <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 30%;background: #0d2d4a;color: #fff;font-size: 9px;" onclick="validar();">
                            Validar
                        </button>
                    </div>
                </center>
            </div>

            <div class="gw-chat-login-cliente-geral-corpo-A-autenticacao-2">
                <center style="margin-top: 10px;float: left;width: 100%;">
                    <div style="width: 100%;">
                        <input type="hidden" name="idCliente" id="idCliente" value="0">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width: 90%;">
                            <input class="mdl-textfield__input" type="text" id="nome" name="nome" style="color: #0d2d4a;">
                            <label class="mdl-textfield__label" for="nome">Nome</label>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width: 90%;">
                            <input class="mdl-textfield__input" type="text" id="email" name="email" style="color: #0d2d4a;">
                            <label class="mdl-textfield__label" for="email">E-mail</label>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width: 90%;display: none;">
                            <input class="mdl-textfield__input" type="text" id="motivo" name="motivo" style="color: #0d2d4a;">
                            <label class="mdl-textfield__label" for="motivo">Motivo</label>
                        </div>
                    </div>
                </center>
                <center style="margin-top: 10px;float: left;width: 100%;">
                    <div style="width: 100%;float: left;margin-bottom: 5px;font-size: 11px;font-weight: bold;color: #666;">
                        <center>
                            <label style="">Escolha o m�dulo que deseja suporte</label>
                        </center>
                    </div>
                    <label class="lb-radio">
                        <input type="radio" name="modulo"  value="trans" />
                        <img src="img/trans_footer.png">
                    </label>
                    <label class="lb-radio">
                        <input type="radio" name="modulo" value="logis"/>
                        <img src="img/logis_footer.png">
                    </label>
                    <label class="lb-radio">
                        <input type="radio" name="modulo" value="frota"/>
                        <img src="img/frota_footer.png">
                    </label>
                    <label class="lb-radio">
                        <input type="radio" name="modulo" value="finan"/>
                        <img src="img/finan_footer.png">
                    </label>
                    <label class="lb-radio">
                        <input type="radio" name="modulo" value="mobile"/>
                        <img src="img/mobile_footer.png">
                    </label>
                    <label class="lb-radio">
                        <input type="radio" name="modulo" value="cloud"/>
                        <img src="img/cloud_footer.png">
                    </label>
                </center>
            </div>
            <div class="gw-chat-login-cliente-geral-corpo-B">
                <center>
                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="background: #0d2d4a;color: #fff;font-size: 10px !important;" onclick="conectar();">
                        Iniciar atendimento
                    </button>
                </center>
            </div>
        </div>
        <form method="POST" action="GwChatControlador?acao=iniciar_chat" id="form">
        </form>
    </body>
</html>
