<%-- 
    Document   : modelo-chat
    Created on : 23/11/2016, 00:08:37
    Author     : Mateus
--%>

<%@page import="br.com.gwchat.geral.Cliente"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <script src="js/jquery.js" type="text/javascript"></script>
        <script src="js/material.min.js" type="text/javascript"></script>
        <link href="css/material.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <script src="js/script.js" type="text/javascript"></script>
        <title>Gw Chat - Suporte t�cnico</title>
        <script>
            var socket = null;
            <%
                Cliente c = (Cliente) request.getAttribute("cliente");
                if (c == null) {
                    RequestDispatcher d = request.getRequestDispatcher("login-cliente");
                    d.forward(request, response);
                }
            %>
            var id = '<%=c.getIdCliente()%>';
            var nome = '<%=c.getNome()%>';
            var cnpj = '<%=c.getCnpj()%>';
            var email = '<%=c.getEmail()%>';
            var modulo = '<%=c.getModulo()%>';

            jQuery(document).ready(function () {
                var md = '';
                if (modulo == 'trans') {
                    md = 'GW Trans';
                } else if (modulo == 'logis') {
                    md = 'GW Logis';
                } else if (modulo == 'frota') {
                    md = 'GW Frota';
                } else if (modulo == 'finan') {
                    md = 'GW Finan';
                } else if (modulo == 'mobile') {
                    md = 'GW Mobile';
                } else if (modulo == 'cloud') {
                    md = 'GW Cloud';
                }
                jQuery('.gw-chat-A-dados-suporte label').html('M�dulo :' + md);
            });

            conectarCliente(id, nome, cnpj, email, modulo);

            function digitando() {
                if (jQuery('.gw-chat-B ul li').last().find('div').attr('class') == "gw-chat-B-cinza" && !javisualizou && socket != null) {
                    socket.send("VYZUALYS0U");
                    javisualizou = true;
                }
            }
        </script>
    </head>
    <body onload="">
        <div class="gw-chat">
            <div class="gw-chat-A">
                <div class="gw-chat-A-topo">
                    <center>Gw Chat - Suporte</center>
                </div>
                <div class="gw-chat-A-buttons">
                    <div class="gw-chat-A-fechar"></div>
                    <div class="gw-chat-A-maxminizar"></div>
                    <div class="gw-chat-A-minimizar"></div>
                </div>
                <div class="gw-img-perfil-suporte">
                    <img src="img/icone-gw.png">
                </div>
                <div class="gw-chat-A-dados-suporte">
                    <span><b>Fila de Espera</b></span>
                    <label>Suporte ao cliente</label>
                </div>
            </div>
            <div class="gw-chat-B">
                <ul>
                </ul>
            </div>
            <div class="gw-chat-B-question">
                <center style="margin-top: 10px;float: left;width: 100%;font-size: 17px;font-weight: bold;color: #666;">
                    Avalia��o do atendente
                </center>
                <center style="float: left;margin-top: 10px;width: 100%;">
                    <div style="width: 100%;float: left;">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width: 90%;">
                            <input class="mdl-textfield__input" type="text" id="observacao" name="observacao" style="color: #0d2d4a;">
                            <label class="mdl-textfield__label" for="observacao" style="">Observa��o sobre o atendimento</label>
                        </div>
                    </div>
                    <div style="width: 100%;float: left;margin-top: 15px;">
                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="gwTrans">
                            <input type="radio" id="gwTrans" class="mdl-radio__button" name="options" value="bom">
                            <span class="mdl-radio__label" style="font-size: 11px;">N�o Satisfat�rio</span>
                        </label>
                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="gwLogis">
                            <input type="radio" id="gwLogis" class="mdl-radio__button" name="options" value="satisfatorio">
                            <span class="mdl-radio__label" style="font-size: 11px;">Satisfat�rio</span>
                        </label>
                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="gwFrota">
                            <input type="radio" id="gwFrota" class="mdl-radio__button" name="options" value="otimo">
                            <span class="mdl-radio__label" style="font-size: 11px;">�timo</span>
                        </label>
                    </div>
                </center>
                <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-1" style="margin-left: 18px;margin-top: 10px;">
                    <input type="checkbox" id="checkbox-1" class="mdl-checkbox__input">
                    <span class="mdl-checkbox__label" style="font-size: 8px;">Gostaria de receber uma c�pia do atendimento por e-mail</span>
                </label>
                <center style="float: left;margin-top: 10px;width: 100%;">
                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="background: #0d2d4a;color: #fff;font-size: 10px !important;" onclick="finalizar();">
                        Finalizar
                    </button>
                </center>
            </div>
            <div class="gw-chat-C">
                <center>
                    <!--<input type="tex" placeholder="Escreva aqui sua mensagem">-->
                    <textarea placeholder="Escreva aqui sua mensagem" class="gw-chat-C-txt-msg" onkeyup="enviarMenssagem(event, this);
                            digitando();"></textarea>
                </center>
                <center>
                    <input type="button" class="gw-chat-C-btn-enviar-msg" value="Enviar Mensagem" onclick="enviarMenssagemClick()">
                </center>
                <input type="checkbox" class="gw-chat-C-chk-enter" id="gw-chat-C-chk-enter" checked><label class="gw-chat-C-lb-chk-enter" for="gw-chat-C-chk-enter">Enter para enviar</label>
            </div>
        </div>
    </body>
</html>
